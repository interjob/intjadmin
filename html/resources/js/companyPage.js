$(document).ready(function(){
    if (accessKey.length > 0 && $('#company_slug').length > 0)
    {
        var input = {};
        input.params = {};
        input.params.company_slug = $('#company_slug').val();
        input.params.accessKey = accessKey;
        input = JSON.stringify(input);

        var defaultData = {
            url: "/authorizeMe",
            methodForm: 'POST',
            beforeSendHandler: false,
            /*	errorHandler 		: 'Form.logOut()',           */
            errorHandler: false,
            successHandler: "Form.loadScript(data,textStatus,jqXHR)",
            completeHandler: false,
            formData: input,
            dataType: "script",
            cache: false,
            contentType: false,
            processData: false,
            crossDomain: false
        };
        Form.ajaxCall(defaultData);
    }
});
    