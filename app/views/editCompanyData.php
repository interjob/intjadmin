<div id="iscriversi" class="content">
   
    <h2><b></b>Modifica i dati della tua azienda tramite il <b>form qui sotto.</b></h2><br /><br />

    <div class="registrazione">
        <form class="registration" name="register_form" id="register_form" action="/updateCompanyData" autocomplete="false" method="post" enctype="multipart/form-data">
                <fieldset class="clearfix">
                                                           
                    <div class="field left clearfix :error_company_name">
                        <label for="company_name">companyName*</label>
                        <input id="company_name" name="company_name" tabindex="1" maxlength="64" class="required" value=":company_name_value" type="text" />
                        <span class="errorLabel">:company_name</span>
                    </div>
                    <div class="field left clearfix :error_company_subtitle">
                        <label for="company_subtitle">company Subtitle*</label>
                        <input id="company_subtitle" name="company_subtitle" tabindex="1" maxlength="64" class="" value=":company_subtitle_value" type="text" />
                        <span class="errorLabel">:company_subtitle</span>
                    </div>

                      <div class="field :error_company_url">
                        <label for="company_url">company_url*</label>
                        <input id="company_url" name="company_url" tabindex="2" maxlength="64" class="required" type="text" value=":company_url_value" />
                        <span class="errorLabel">:company_url</span>
                    </div>
                    
                      <div class="field :error_company_baseinfo">
                        <label for="company_baseinfo">company_baseinfo*</label>
                        <textarea id="company_baseinfo" name="company_baseinfo" tabindex="2" maxlength="64" class="required" type="text" >:company_baseinfo_value</textarea>                 
                        <span class="errorLabel">:company_baseinfo</span>
                    </div>
                    
                      <div class="field :error_company_services">
                        <label for="company_baseinfo">company_services</label>
                        <textarea id="company_services" name="company_services" tabindex="2" maxlength="64" class="" type="text" >:company_services_value</textarea>                 
                        <span class="errorLabel">:company_services</span>
                    </div>
                    
                      <div class="field :error_company_description">
                        <label for="company_description">company_description*</label>
                        <textarea id="company_description" name="company_description" tabindex="2" maxlength="64" class="required" type="text" >:company_description_value</textarea>
                        <span class="errorLabel">:company_description</span>
                    </div>
                    
                      <div class="field :error_social_facebook_link">
                        <label for="social_facebook_link">social_facebook_link*</label>
                        <input id="social_facebook_link" name="social_facebook_link" tabindex="2" maxlength="64" class="" type="text" value=":social_facebook_link_value" />
                        <span class="errorLabel">:social_facebook_link</span>
                    </div>                    
                    
                    <div class="field :error_social_linkedin_link">
                        <label for="social_linkedin_link">social_linkedin_link*</label>
                        <input id="social_linkedin_link" name="social_linkedin_link" tabindex="2" maxlength="64" class="" type="text" value=":social_linkedin_link_value" />
                        <span class="errorLabel">:social_linkedin_link</span>
                    </div>        
                    
                    <!-- new social -->
                    
                    <div class="field :error_social_500px_link">
                        <label for="social_500px_link">social_500px_link*</label>
                        <input id="social_500px_link" name="social_500px_link" tabindex="2" maxlength="64" class="" type="text" value=":social_500px_link_value" />
                        <span class="errorLabel">:social_500px_link</span>
                    </div>                     
                    
                    <div class="field :error_social_flicker_link">
                        <label for="social_flicker_link">social_flicker_link*</label>
                        <input id="social_flicker_link" name="social_flicker_link" tabindex="2" maxlength="64" class="" type="text" value=":social_flicker_link_value" />
                        <span class="errorLabel">:social_flicker_link</span>
                    </div>                     
                    
                    <div class="field :error_social_google_plus_link">
                        <label for="social_google_plus_link">social_google_plus_link*</label>
                        <input id="social_google_plus_link" name="social_google_plus_link" tabindex="2" maxlength="64" class="" type="text" value=":social_google_plus_link_value" />
                        <span class="errorLabel">:social_google_plus_link</span>
                    </div>                     
                    
                    <div class="field :error_social_instagram_link">
                        <label for="social_instagram_link">social_instagram_link*</label>
                        <input id="social_instagram_link" name="social_instagram_link" tabindex="2" maxlength="64" class="" type="text" value=":social_instagram_link_value" />
                        <span class="errorLabel">:social_instagram_link</span>
                    </div>                     
                    
                    <div class="field :error_social_pinterest_link">
                        <label for="social_pinterest_link">social_pinterest_link*</label>
                        <input id="social_pinterest_link" name="social_pinterest_link" tabindex="2" maxlength="64" class="" type="text" value=":social_pinterest_link_value" />
                        <span class="errorLabel">:social_pinterest_link</span>
                    </div>                     
                    
                    <div class="field :error_social_soundcloud_link">
                        <label for="social_soundcloud_link">social_soundcloud_link*</label>
                        <input id="social_soundcloud_link" name="social_soundcloud_link" tabindex="2" maxlength="64" class="" type="text" value=":social_soundcloud_link_value" />
                        <span class="errorLabel">:social_soundcloud_link</span>
                    </div>                     
                    
                    <div class="field :error_social_tumblr_link">
                        <label for="social_tumblr_link">social_tumblr_link*</label>
                        <input id="social_tumblr_link" name="social_tumblr_link" tabindex="2" maxlength="64" class="" type="text" value=":social_tumblr_link_value" />
                        <span class="errorLabel">:social_tumblr_link</span>
                    </div>                     
                    
                    <div class="field :error_social_twitter_link">
                        <label for="social_linkedin_link">social_twitter_link*</label>
                        <input id="social_twitter_link" name="social_twitter_link" tabindex="2" maxlength="64" class="" type="text" value=":social_twitter_link_value" />
                        <span class="errorLabel">:social_twitter_link</span>
                    </div>                     
                    
                    <div class="field :error_social_vimeo_link">
                        <label for="social_vimeo_link">social_vimeo_link*</label>
                        <input id="social_vimeo_link" name="social_vimeo_link" tabindex="2" maxlength="64" class="" type="text" value=":social_vimeo_link_value" />
                        <span class="errorLabel">:social_vimeo_link</span>
                    </div>                     
                    
                    <div class="field :error_social_youtube_link">
                        <label for="social_youtube_link">social_youtube_link*</label>
                        <input id="social_youtube_link" name="social_youtube_link" tabindex="2" maxlength="64" class="" type="text" value=":social_youtube_link_value" />
                        <span class="errorLabel">:social_youtube_link</span>
                    </div>                     
                    
                    
                    <!-- end new social -->
                                    
                      <div class="field :error_company_keywords">
                        <label for="company_keywords">company_keywords* inserirle divise da un ; </label>
                        <input id="company_keywords" name="company_keywords" tabindex="2" maxlength="64" class="required" type="text" value=":company_keywords_value" />
                        <span class="errorLabel">:company_keywords</span>
                    </div>                       
                    
                    <input type="hidden" name="company_slug" id="companySlug" value=":company_slug" />      
                    
                </fieldset>
            

                <div class="field service"></div>
                <p style="font-size:13px;">Tutti i campi con * sono obbligatori.</p>
                <a href="#" class="button registrar">Salva</a>
                <div class="loader"></div>
        </form>

        <!-- CONFERMA AVVENUTA ISCRIZIONE -->
        <h3 class="successful">:successMessage</h3>
        <!-- La tua registrazione è andata a buon fine. Tra poco riceverai una mail di conferma contenente le istruzioni. -->

        <!-- ERRORE REGISTRAZINOE -->
        <h3 class="mistake">:failureMessage</h3>
    </div>
    <div class="clear"></div>
</div>

