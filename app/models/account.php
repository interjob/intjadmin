<?php

class account extends baseModel
{
	//* public static method to insert account
	/**
		this method insert new account with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
					'authuser_id'           => 1,
                                        'name'                  => 'Ziummo',
                                        'surname'               => 'Scarso',
                                        'birthplace'            => 'Culandia',
                                        'birthdate'             => 27/02/1989,
                                        'sex'                   => 'M',
					'profile_img'           => 'pat/to/file',
                                        'address' 		=> 'via le mani',
                                        'address_civic'         => '2',
                                        'zipcode'               => '22666',
                                        'city'                  => 'Paperopoli',
                                        'states'                => 'MB',
					'country'               => 'Italy',
                                        'vatcode'               => 'BNFGVNNL44F704U',
                                        'privacy'               => 1,
                                        'newletter'             => 1,
					'createdat'             => '19900219 14:22:45',
					'updatedat' 		=> '19900219 14:22:45',
                                        'active'                => 1,
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function insertAccount($db, $params, $userID)
	{
		$rs = self::get($db, $params, 'accounts', $userID);
                
		if(isset($rs[0]))
			return ALREADY_IN;
		else
			return parent::insert($db, $params, 'accounts', 'id', $userID);
	}

	//* public static method to update account
	/**
		this method update existing account with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
					'authuser_id'           => 1,
                                        'name'                  => 'Ziummo',
                                        'surname'               => 'Scarso',
                                        'birthplace'            => 'Culandia',
                                        'birthdate'             => 27/02/1989,
                                        'sex'                   => 'M',
					'profile_img'           => 'pat/to/file',
                                        'address' 		=> 'via le mani',
                                        'address_civic'         => '2',
                                        'zipcode'               => '22666',
                                        'city'                  => 'Paperopoli',
                                        'states'                => 'MB',
					'country'               => 'Italy',
                                        'vatcode'               => 'BNFGVNNL44F704U',
                                        'privacy'               => 1,
                                        'newletter'             => 1,
					'createdat'             => '19900219 14:22:45',
					'updatedat' 		=> '19900219 14:22:45',
                                        'active'                => 1,
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function updateAccount($db, $params, $conditions, $userID)
	{
		return parent::update($db, $params, 'accounts', $conditions, $userID);
	}

	//*public static method to delete account
	/**
		this method delete one entity row
		$db : PDO resource to connect and retrieve data
		$id : mandatory for find what to delete
		$userID : it's identifier of user that do this action
		return true or false if the operation do its job or not
	*/
	static public function deleteAccount($db, $params = array(), $userID)
	{
		return parent::delete($db, $params, 'account', $userID);
	}

	//*public static method to get account
	/**
		this method return entity list or if id parameter is not null get details of one account entity
		$db : PDO resource to connect and retrieve data
		$id : optional parameter, if it is set get single account entity
		$userID : it's identifier of user that do this action
		return list of account or single account
	*/
	static public function getAccount($db, $params = array(), $userID)
	{
		return parent::get($db, $params, 'accounts', $userID);
	}

}
