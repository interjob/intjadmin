<?php

class profileType extends baseModel
{
	//* public static method to insert profile_type
	/**
		this method insert new profile_type with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
					'code_profile'              => 'free',
                                        'title'                     => 'profiles_types_title_free',
                                        'description'               => 'profiles_types_description_free',                                        
                                        'icon'                      => 'path/to/image',                                        
					'createdat'                 => '19900219 14:22:45',
					'updatedat'                 => '19900219 14:22:45',
                                        'active'                    => 1,
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function insertProfileType($db, $params, $userID)
	{
		$rs = self::get($db, $params, 'profiles_types', $userID);

		if(isset($rs[0]))
			return ALREADY_IN;
		else
			return parent::insert($db, $params, 'profiles_types', 'id', $userID);
	}

	//* public static method to update profile_type
	/**
		this method update existing profile_type with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
					'code_profile'              => 'free',
                                        'title'                     => 'profiles_types_title_free',
                                        'description'               => 'profiles_types_description_free',                                        
                                        'icon'                      => 'path/to/image',                                        
					'createdat'                 => '19900219 14:22:45',
					'updatedat'                 => '19900219 14:22:45',
                                        'active'                    => 1,
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function updateProfileType($db, $params, $conditions, $userID)
	{
		return parent::update($db, $params, 'profiles_types', 'id', $conditions, $userID);
	}

	//*public static method to delete profile_type
	/**
		this method delete one entity row
		$db : PDO resource to connect and retrieve data
		$id : mandatory for find what to delete
		$userID : it's identifier of user that do this action
		return true or false if the operation do its job or not
	*/
	static public function deleteProfileType($db, $params = array(), $userID)
	{
		return parent::delete($db, $params, 'profiles_types', $userID);
	}

	//*public static method to get profile_type
	/**
		this method return entity list or if id parameter is not null get details of one profile_type entity
		$db : PDO resource to connect and retrieve data
		$id : optional parameter, if it is set get single profile_type entity
		$userID : it's identifier of user that do this action
		return list of profile_type or single profile_type
	*/
	static public function getProfileType($db, $params = array(), $userID)
	{
		return parent::get($db, $params, 'profiles_types', $userID);
	}

}
