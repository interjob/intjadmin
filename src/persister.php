<?php
class MyPDO extends PDO{

	function __construct($connectorParameters, $username, $password){
		//$this->db = $db;
		parent::__construct($connectorParameters, $username, $password);
	}

	public function prepareParameters($data){
		$newData = array();
		foreach ($data as $key => $value) {
			$newData[':'.$key] = $value;
		}
		return $newData;
	}

	public function prepareUpdateConditionsAndSets($data, $conditions = false)
	{
		$newData = "";
		foreach ($data as $key => $value) {
			if(strlen($newData) > 0)
			{
				if($conditions === false)
					$newData.=", ";
				else
					$newData.=" AND ";
			}
			if($conditions === false)
				$newData.="$key = :$key";
			else
				$newData.="$key = '$value'";
		}

		return $newData;
	}
        
        static public function myExec($db, $query, $paramsQuery, $authuserID, $logIt = true)
	{
            
                //if(strstr($query, 'INSERT') !== FALSE)
            
            /*
               if (strstr($query, "UPDATE"))
                {
		 var_dump($query);
		 var_dump($paramsQuery);
                 //die();
                }
                */
		$stmt = $db->prepare($query);
		$stmt->execute($paramsQuery);
                
	//	print_r($db->errorInfo());  //for error query
                
                if(strstr($query, 'SELECT') !== FALSE)
                {
                    $rs = $stmt->fetchAll(PDO::FETCH_ASSOC);
                }
                else 
                {
                    $rs = $stmt->rowCount();
                }
                if(DEBUG)
                {
                    if($logIt)
			Log::logIt($db, $query, $authuserID);
                }
		return $rs;
	}
                
        //*private method to init or reinit db
	/**
		reset initialize or reinitialize db from sql prepared, it is call only in DEBUG mode
	*/

	public function reset(){
		$sql = file_get_contents(BASE_PATH."/../src/resetdb.sql");
		$stmt = self::prepare($sql);
		$stmt->execute();
                header('Location: '.$_SERVER['REQUEST_URI']);
	}
	//TODO SPECIAL METHOD
}