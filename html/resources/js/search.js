$( document ).ready(function() {
       Form.listCategory();     
});
Form.resultNumber = 15;


$(".search-form" ).submit(function( event ) {
      event.preventDefault();  
      $("#revo-slider").slideUp( function(){
         $(".searchSection").slideDown(); 
      });      
      
      $(".companyBadge").not(":first").remove();
      //console.log(badgePublic);
      

                var input = {};
                input.params={};
                input.params.company_name =  $('#company_name').val();
                if($('#route').val().length > 0)
                    input.params.company_address =  $('#route').val()+ ", "+$('#street_number').val();
                else
                    input.params.company_address = "";
                input.params.company_city =  $('#locality').val();
                input.params.company_state =  $('#administrative_area_level_2').val();          //provincia
                input.params.company_cap =  $('#postal_code').val();
                input.params.company_country =  $('#country').val();                
                input.params.latgeo =  $('#latgeo').val();
                input.params.longgeo =  $('#longgeo').val();
                                    
    
      
       input=JSON.stringify(input);
       
    //console.log(input);       

        var defaultData ={
                url 				: $(this).attr("action"),
                methodForm 			: 'POST',
                beforeSendHandler               : false,
        /*	errorHandler                    : 'alert("error removing company")',   */          
                errorHandler                    : 'Form.errorHandler',                
                successHandler                  : 'Form.submitNoErrors(data,textStatus,jqXHR)',                        
                completeHandler                 : false,
                formData			: input,
                dataType			: "json",
                cache				: false,
                contentType			: false,
                processData			: false,
                crossDomain			: false
        };

        Form.ajaxCall(defaultData); 

});





Form.submitNoErrors = function(data, textStatus, jqXHR, target){
    //console.log(textStatus);   
    //alert(textStatus);
  /*  if(!data.success){
        alert("xx");
    }
    else
    {
       $("#"+target).remove();
    }*/
    

        //  console.log(data);

          
          //$.cookie("DataSearch", data.accessKey,{ expires : 1 });           
          
            // Put the object into storage
            localStorage.setItem('DataSearch', JSON.stringify(data.data));

            // Retrieve the object from storage
            Form.appendBadge(0);


            if (data.data.length>Form.resultNumber)
            {
                console.log($("footer").height());
                console.log($(document).height());
                console.log($(window).height());
                $(window).scroll(function () {
                    if ($(window).scrollTop() === $(document).height() - $(window).height()) {
                        Form.appendBadge( $(".companyNameBadge").length-1);
                    }
                });
            }
          
};


/*
$("#pigia").click(function(){
    appendBadge( $(".companyNameBadge").length-1);
});

*/

Form.appendBadge = function(start){

            var retrievedObject =  JSON.parse(localStorage.getItem('DataSearch'));
            //console.log(retrievedObject);            
            var badgePublic = "";    
            var html = "";    
             
            var ActualNumber= $(".companyNameBadge").length;
                            
            for (i=start; i < ActualNumber+Form.resultNumber-1; i++){
                
                if (i < retrievedObject.length){
                    
                    if (retrievedObject[i].company_name){
                            badgePublic = $(".templatebadge").clone();
                            $(badgePublic).removeClass("templatebadge");
                            $(badgePublic).removeClass("hide");
                            $(badgePublic).attr("id", retrievedObject[i].company_slug);
                            $(badgePublic).find(".companyNameBadge").text(retrievedObject[i].company_name);
                            $(badgePublic).find(".companyLinkBadge").attr("href","/companies/"+retrievedObject[i].company_slug+".html");
                            $(badgePublic).find(".companyAddressBadge").text(retrievedObject[i].city+" - "+retrievedObject[i].state);

                            if (retrievedObject[i].profile_id > 1){
                                $(badgePublic).find(".companyLinkBadge").attr("href","/companies/"+retrievedObject[i].company_slug+".html");
                            }else{
                                $(badgePublic).find(".companyLinkBadge").addClass("hide");                        
                            }


                            html = $(badgePublic).html();                   
                            $(badgePublic).appendTo("#companyList ul"); 
                    }
                    
                    
                }else{
                    break;
                }
            }
            
            
          
            //$("#companyList ul").append("<div>----</div>");
            //});

}     