
Form.checkInput = function(form){
	//JS validation
	$(form).find('input,textarea,checkbox,radio,select').each(function(key,element) {
		if($(element).hasClass('required')){
			switch($(element).attr('type')){
				case 'checkbox':
							if(!$(element).is(':checked'))
					        	$(element).parent('div').addClass('error');
							break;
				case 'radio':
							if(!$(element).is(':checked'))
					        	$(element).parent('div').addClass('error');
                                                    
							break;
				case 'email':
                                                        if (!Form.isValidEmailAddress($(element).val()))                                                                  
                                                             $(element).parent('div').addClass('error');  

							break;
				case 'password':
							if($("#password").val()!=$("#password-recheck").val()){
                                                            $(element).parent('div').addClass('error');                                                            
                                                          //  $("#password").addClass('error');
                                                          //  $("#password-recheck").addClass('error');
                                                        }
                                                        break;
				case 'file':
							/*if(!$.browser.msie){
								if(!Form.validateImg(element.files[0])){
									$(element).val('');
						        	$(element).parent('div').addClass('error');
						        }
						    }*/
							break;
				default:
							if($(element).val()=='')
								$(element).parent('div').addClass('error');
							break;

			}
		}

    });

};


Form.submitNoErrors = function(data, textStatus, jqXHR){
        console.log(textStatus);
      /*  switch(textStatus){
            case :
        }*/
    
	if(!data.success){
    	$.each(data.errors, function(key, value){
    		$('#' + key).parent('.field').addClass('error');
    		$('<span class="errorLabel">' + value + '</span>').insertAfter('#' + key)
    	});
    	Form.attachEventOne(jQuery('.registrar'), 'click', 'Form.submit($(this).closest(\'form\'));');
    	$('.loader').delay(200).fadeOut('fast');
    	$('.registrar').delay(400).slideDown('fast');
    }
    else
    {
    	Form.attachEventOne(jQuery('.registrar'), 'click', '');
    	Form.successRegister(data.message);
    }
};


Form.successRegister = function(message){
	$('#register_form').slideUp('fast');
	$('.registrazione .successful').text(message);
	$('.registrazione .successful').slideDown('fast');
};

/*
Form.errorRegister = function(data){
	$('#register_form').slideUp('fast');
	$('.registrazione .mistake').show('fast');
        $(".mistake").html($(".mistake").html()+" "+data.errors.critical);
};
*/

Form.dataAcquisionAccount= function (){
        
    //if insert
    if ($("#accountSlug").val()=="insert"){
        
        console.log("insert form");
        var formInput = new FormData();
        formInput.append( 'name', $('#name').val());                
        formInput.append( 'surname', $('#surname').val());                
        formInput.append( 'email', $('#email').val());                
        formInput.append( 'password', $('#password').val());                
        formInput.append( 'birthdate', $('#birthdate').val());                
        formInput.append( 'birthplace', $('#birthplace').val());   

        var Sex="";
        if ($("#inlineMale").is(':checked'))
            Sex="M";
        if ($("#inlineFemale").is(':checked'))
            Sex="F";    

        formInput.append( 'sex', Sex);                
        formInput.append( 'phone', $('#phone').val());                
        formInput.append( 'address', $('#address').val());                
        formInput.append( 'address_civic', $('#address_civic').val());                
        formInput.append( 'zipcode', $('#zipcode').val());                
        formInput.append( 'city', $('#city').val());                
        formInput.append( 'states', $('#states').val());                
        formInput.append( 'country', $('#registration_country').val());                
        formInput.append( 'vatcode', $('#vatcode').val());            

         var privacy=0;
        if ($("#privacy").is(':checked'))
            privacy=1;
          var newsletter=0;                
        if ($("#newsletter").is(':checked'))
            newsletter=1;                    
        formInput.append( 'privacy', privacy);                
        formInput.append( 'newsletter', newsletter);      
        
    }else{
        console.log("modify form");        
        var formInput = new FormData();
        formInput.params ={};        
        
        var Sex="";
        if ($("#inlineMale").is(':checked'))
            Sex="M";
        if ($("#inlineFemale").is(':checked'))
            Sex="F";   
         var privacy=0;
        if ($("#privacy").is(':checked'))
            privacy=1;
          var newsletter=0;                
        if ($("#newsletter").is(':checked'))
            newsletter=1;          
        
        formInput.params.name=$('#name').val();
        formInput.params.surname=$('#surname').val();
        //formInput.params.email=$('#email').val();
        formInput.params.password=$('#password').val();
        formInput.params.birthdate=$('#birthdate').val();
        formInput.params.birthplace=$('#birthplace').val();        
        formInput.params.sex=Sex;
        formInput.params.phone=$('#phone').val();
        formInput.params.address=$('#address').val();
        formInput.params.address_civic=$('#address_civic').val();
        formInput.params.zipcode=$('#zipcode').val();
        formInput.params.city=$('#city').val();
        formInput.params.states=$('#states').val();
        formInput.params.country=$('#registration_country').val();
        formInput.params.vatcode=$('#vatcode').val();       
        formInput.params.privacy=privacy;        
        formInput.params.newsletter=newsletter;        
        formInput.params.accessKey=accessKey;        
        formInput=JSON.stringify(formInput);              
                             
    }


             
    return formInput; 
};


Form.submitAJAX = function(form) {
	/*if($.browser.msie){
		$('#register_form').attr('action', '/success');
		$('#register_form').submit();
	}
	else{*/
            
           /* $("#register_form").submit();*/
            
            
               // var formInput = new FormData($(form)[0]);         //get all form data
                              
               var formInput = Form.dataAcquisionAccount();
                //formInput.params ={};
                //formInput.params.accessKey = accessKey;
                console.log(formInput);   
          


		var defaultData ={
			url 				: $(form).attr('action'),
			methodForm 			: 'POST',
			beforeSendHandler 	: false,
	/*		errorHandler 		: 'Form.errorRegister(data)',  */  
			errorHandler 		: 'Form.errorHandler',                                              
			successHandler 		: 'Form.submitNoErrors(data,textStatus,jqXHR)',
			completeHandler		: false,
			formData			: formInput,
			dataType			: "json",
			cache				: false,
			contentType			: false,
			processData			: false,
			crossDomain			: false
		};

		Form.ajaxCall(defaultData);            
            
            
            return false;
		/*
                var formInput = new FormData($(form)[0]);                
		var defaultData ={
			url 				: $(form).attr('action'),
			methodForm 			: 'POST',
			beforeSendHandler 	: false,
			errorHandler 		: 'Form.errorRegister',
			successHandler 		: 'Form.submitNoErrors(data, textStatus, jqXHR)',
			completeHandler		: false,
			formData			: formInput,
			dataType			: "json",
			cache				: false,
			contentType			: false,
			processData			: false,
			crossDomain			: false
		};

		Form.ajaxCall(defaultData);*/
	//}

};



Form.submit = function(form){
	$('.errorLabel').remove();
	$('.field').removeClass('error');

	Form.checkInput(form);

	//Form.AJAXValidation(form);

	if($('.error').length == 0){
		$('.registrar').slideUp('fast');
		$('.loader').fadeIn('fast');
		Form.attachEventOne(jQuery('.registrar'), 'click', '');
		Form.submitAJAX(form);
	}
	else
            Form.attachEventOne(jQuery('.registrar'), 'click', 'Form.submit($(this).closest(\'form\'));');
};

jQuery(document).on('ready', function(){
    
        $(".changeSex").change(function(){
            $("#sex").val($(this).val()); 
        });
        
	Form.attachEventOne(jQuery('.registrar'), 'click', 'Form.submit($(this).closest(\'form\'));');
	Form.attachEventOne(jQuery('.submitContact'), 'click', 'Form.contactSubmit($(this).closest(\'form\'))');
});