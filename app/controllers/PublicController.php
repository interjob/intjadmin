<?php
class PublicController extends BaseController
{    
    private $errors = array();
    
    function __construct($app, $db, $parameters = '')
    {
            parent::__construct($app, $db);
            if(!empty($parameters))
            {
                    $this->parameters = $parameters;
                    if(isset($this->parameters['accessKey']))
                            $this->key = $this->parameters['accessKey'];
            }
            
            if(isset($parameters['errors']))
                $this->errors = unserialize(base64_decode($parameters['errors']));            
    }
    
    public function home()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'home'		=> array(),
			'footer' 	=> array(),
		);
        $this->render('home', array(), $staticContent);
    }
    
    public function login()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleLogin'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'login'		=> array(),
			'footer' 	=> array("script"=>"login.js"),
		);
        $this->render('login', array(), $staticContent);
    }
    
    
    //STATIC PAGES 
    
    public function personal()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'personal'	=> array(),
			'footer' 	=> array(),
		);
        $this->render('personal', array(), $staticContent);
    }     
    
    public function company()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'company'	=> array(),
			'footer' 	=> array(),
		);
        $this->render('company', array(), $staticContent);
    }     
    
    public function reseller()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'reseller'	=> array(),
			'footer' 	=> array(),
		);
        $this->render('reseller', array(), $staticContent);
    }     
    
    public function swap()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'swap'	=> array(),
			'footer' 	=> array(),
		);
        $this->render('swap', array(), $staticContent);
    }         
    
    public function promo()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'promo'         => array(),
			'footer' 	=> array(),
		);
        $this->render('promo', array(), $staticContent);
    }         
    
    public function work()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'work'         => array(),
			'footer' 	=> array(),
		);
        $this->render('work', array(), $staticContent);
    }         
     
    public function privacy()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'privacy'	=> array('privacy_text' => $this->translation["privacy_text"], 'style'=> 'style'),
			'footer' 	=> array(),
		);
        $this->render('privacy', array(), $staticContent);
    }    
    
    public function sitemap()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'sitemap'	=> array(),
			'footer' 	=> array(),
		);
        $this->render('sitemap', array(), $staticContent);
    }    
    
    public function about_us()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'about_us'	=> array(),
			'footer' 	=> array(),
		);
        $this->render('about_us', array(), $staticContent);
    } 
    
    public function services()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'services'	=> array(),
			'footer' 	=> array(),
		);
        $this->render('services', array(), $staticContent);
    }
    
    public function faq()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'faq'	=> array(),
			'footer' 	=> array(),
		);
        $this->render('faq', array(), $staticContent);
    }    
    
    public function terms_and_conditions()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'terms_and_conditions'	=> array(),
			'footer' 	=> array(),
		);
        $this->render('terms_and_conditions', array(), $staticContent);
    }    
    
    public function credits()
    {
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'credits'	=> array(),
			'footer' 	=> array(),
		);
        $this->render('credits', array(), $staticContent);
    }    
    
    
    
//dinamic pages
    
    public function search()
    {
        /*
        //get all company with active = 1
        $companies = Company::getCompanyOrdered($this->db, array('active'=>1), 0);
        
        //compose list result
        $template_company_list="";
            foreach ($companies as $company) {
                                             
                $template_company_list.= $this->getView('companyBadgePublic', array('company_name' => $company['company_name'], 'company_slug'=>$company["company_slug"]));
            }        
       */
        $staticContent = array(
			'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
			'search'        => array(),
                        'footer'        => array("script"=>"company.js"),
		);
        $this->render('search', array(), $staticContent);
    }         
    
    
    public function categoryList(){
        
        $companyTipologies = CompanyTipology::getTranslatedompanyTipology($this->db, array(), $this->language->language, 0);
    
        if (!empty($companyTipologies)){
            $this->jsonResponse(array('success' => true, 'message' => $this->translation['OperationSuccess'], 'data' => $companyTipologies), 200, $this->key);
        }else{
            $this->jsonResponse(array('success' => false, 'message' => $this->translation['NoResultFound']), 200, $this->key);
        }
        
    }
    
    
    public function cropPopOver()
    {
        echo $this->getView("cropPopOver", array());
    }    
        
    
    
    public function searchResult()
    {
        
        //data acquisition            
        $data = $this->app->request()->params();
        $data = array_merge($data, $this->parameters);
       
        /*
         $Limit=MAXSEARCHRESULT;
         
         if (!$end){ 
             $start=0;             
         }else{
             $start=$data["start"];
         }        
        $end=$Limit+$start;
        $limitQuery.=" LIMIT ".intval($start)." , ".intval($end);        
         */
        
                
        //addresses logic
        $queryParams = array();
        $sWhereLocation="";
        if (!empty($data["company_country"])){          
            //$sWhereLocation=" AND companies_addresses.country = '".$data["company_country"]."'";
            $queryParams["company_country"] = $data["company_country"];
        } 
        
        if (!empty($data["company_state"])){          
            //$sWhereLocation=" AND companies_addresses.state = '".$data["company_state"]."'";
            $queryParams["company_country"] = $data["company_state"];        
        }         
        
        if (!empty($data["company_city"])){          
            $queryParams["company_city"] = $data["company_city"];
            //$sWhereLocation=" AND companies_addresses.city = '".$data["company_city"]."'";
        } 
        
        if (!empty($data["company_cap"])){          
            $queryParams["company_cap"] = $data["company_cap"];
            //$sWhereLocation=" AND companies_addresses.cap = '".$data["company_cap"]."'";
        }     
        
        if (!empty($data["company_address"])){
            $queryParams["company_cap"] = $data["company_address"]."%";
            $queryParams["company_cap"] = $data["company_city"];
            //$sWhereLocation=" AND companies_addresses.city = '".$data["company_city"]."'";            
            //$sWhereLocation.=" AND companies_addresses.address LIKE '".$data["company_address"]."%'";
        }  
        
        if (!empty($data["company_number"])){
            //query diretta specifica per via senza ulteriori menate
            //ma sarebbe da fare una ricerca di zona limitrofa            
        }        
        
        $sWhere = ""; 
        $sWhere.=$sWhereLocation;
        $joinCat="";
        
        if (isset($data["categoriesSearch"])){           
            $joinCat = " LEFT JOIN company_typologies_link ON company_typologies_link.id_company = companies.id ";
            $queryParams["company_typologies_link.id_typology"] = $data["categoriesSearch"];
            //$sWhere.=" AND company_typologies_link.id_typology = ".$data["categoriesSearch"]." ";            
        }             
        $sWhere = $this->db->prepareUpdateConditionsAndSets($queryParams, false); 
        if (isset($data["keywordsSearch"])){            
            
            $sWhere.=" AND ((companies.company_keywords LIKE '%".mysql_real_escape_string($data["keywordsSearch"])."%') OR (companies.company_name LIKE '%".mysql_real_escape_string($data["keywordsSearch"])."%'))";
        }
        if(strlen($sWhere)>0)
            $sWhere.= " AND ";
        $sWhere.= " companies.active = 1 AND companies_addresses.active = 1 ";
                                     
         $select=" 
                    companies.company_slug, 
                    companies.company_name, 
                    companies.profile_id, 
                    companies_addresses.address, 
                    companies_addresses.city, 
                    companies_addresses.state, 
                    companies_addresses.cap, 
                    companies_addresses.country 
             ";         
                
        $query =" SELECT ".$select." FROM companies ".$joinCat." LEFT JOIN companies_addresses ON companies.id = companies_addresses.company_id WHERE ".$sWhere." ORDER BY companies.profile_id DESC " ;
        
        //var_dump($data);
        //var_dump($query);        
        $companies= MyPDO::myExec($this->db, $query, $queryParams, 0);       
        
        if (!empty($companies)){
            $this->jsonResponse(array('success' => true, 'message' => $this->translation['OperationSuccess'], 'data' => $companies), 200, $this->key);
        }else{
            $this->jsonResponse(array('success' => false, 'message' => $this->translation['NoResultFound']), 200, $this->key);
        }  
        
       //var_dump($template_company_list);
       
     /*   if (empty($template_company_list)) 
            $template_company_list = $this->translation['noResult'];
          */  
        /*
        $staticContent = array(
                'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
                'menu'		=> array(),
                'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
                'search' 	=> array("matrixCompany"=>$template_company_list),
                'footer' 	=> array(),
        );
        $this->render('search', array(), $staticContent);            
        */
        
        
      //  array(9) { ["company_number"]=> string(0) "" ["company_address"]=> string(13) "Via Aeroporto" ["company_city"]=> string(13) "Orio Al Serio" ["company_country"]=> string(6) "Italia" ["company_cap"]=> string(5) "24050" ["latgeo"]=> string(10) "45.6729409" ["longgeo"]=> string(17) "9.687929899999972" ["categoriesSearch"]=> string(12) "categoria..." ["keywordsSearch"]=> string(6) "dfsfsd" }
        


        
        
        
        //mandare risposta in ajax indietro al js, elenco completo json?
        
    

           
    }
    
    
//registration and login    
    
    public function confirmAccount()
    {
        //TODO REMEMBER TO CREATE AUTHTOKEN INSERT IN AUTH MODEL
        if(!isset($this->parameters['encodedEmail']))
        {
            $this->app->redirect('/not-found');
            return;
        }
        else
        {
            $data['email'] = base64_decode($this->parameters['encodedEmail']);
            $validator = array(
                'email' => array('required', 'email')
            );
            $validation = new errorHandling($this->translation, $validator);
            $validationRs = $validation->isValid($data);
            if(!empty($validation->errors))
            {
                $this->app->redirect('/not-found');
                return;                
            }
            else
            {
                $user = Auth::getAuthUser($this->db, $data, 0);
                if(!isset($user[0]['id']))
                {
                    $this->app->redirect('/not-found');
                    return;
                }
                else
                {
                    $rsToken = Auth::insertAuthToken($this->db, $user[0]['id']);
                    if($rsToken == ALREADY_IN)
                    {
                        $pageParameters = array(
                            'status'    =>  'error',
                            'message'   =>  $this->translation['accountAlreadyVerified']
                        );
                    }
                    else
                    {
                        $pageParameters = array(
                            'status'    =>  'success',
                            'message'   =>  $this->translation['accountCorrectlyVerified']
                        );
                    }
                }
                
            }
            $staticContent = array(
                    'head' 		=> array('title' => $this->translation['titleAccountValidation'], 'style' => 'style'),
                    'menu'		=> array(),
                    'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
                    'confirmAccount'    => $pageParameters,
                    'footer'            => array(),
            );
            $this->render('confirmAccount,', array(), $staticContent);
        }

        
    }
    
    public function registration()
    {
        //this validation is only for sync actions, this project doesn't have
        $errors = array(
                        'first_name' 	=> '',
                        'last_name' 	=> '',
                        'company' 		=> '',
                        'profession' 	=> '',
                        'mail' 			=> '',
                        'privacy' 		=> '',
                        'photo' 		=> '',
                        );

        $errors = array_merge($errors, $this->errors);

        foreach($errors as $key => $error):
                if(!empty($error)){
                        $errors['error_'.$key] = 'error';
                }
                else
                        $errors['error_'.$key] = '';
        endforeach;
        //end old validation
        
        $staticContent = array(
                'head' 		=> array('title' => $this->translation['titleHome'], 'style' => 'style'),
                'menu'		=> array(),
                'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
                'registration'  => array("failureMessage"=> $this->translation['failureMessage']),
                'footer' 	=> array("script"=>"registration.js"),
        );
        $this->render('registration', array(), $staticContent);
    }
    
    public function lostPassword()
    {
        $errors = array( 'username' => '');

        $errors = array_merge($errors, $this->errors);

        foreach($errors as $key => $error):
                if(!empty($error)){
                        $errors['error_'.$key] = 'error';
                }
                else
                        $errors['error_'.$key] = '';
        endforeach;
        $staticContent = array(
                'head' 		=> array('title' => $this->translation['titleLostPassword'], 'style' => 'style'),
                'menu'		=> array(),
                'header' 	=> array('placeToSearch'=> $this->translation["placeToSearch"], 'categoriesSearch'=> $this->translation["categoriesSearch"],'keywordsSearch'=> $this->translation["keywordsSearch"] ),
                'lostPassword'  => $errors,
                'footer' 	=> array(),
        );
        $this->render('lostPassword', array(), $staticContent);  
    }

    public function getNewPassword()
    {
        $data = $this->app->request()->params();
        $data = array_merge($data, $this->parameters);
        
        $validator = array(
            'username'  => array('required', 'email'),
        );        
        
        $validation = new errorHandling($this->translation, $validator);
        $validationRs = $validation->isValid($data);

        if(isset($validation->errors['critical'])){			

               // $response->body(json_encode(array('success' => false,  'errors' => $validation->errors)));

                $this->jsonResponse(array("success" => false, "errors" => $validation->errors, "message"=>$this->translation['OperationFailed']) ,406, $this->key);                     
                return;
        }

        if(!empty($validation->errors)){			

                //$response->body(json_encode(array('success' => false,  'errors' => $validation->errors)));
                $this->jsonResponse(array("success" => false, "errors" => $validation->errors, "message"=>$this->translation['OperationFailed']) ,200, $this->key);                 
                return;
        }
        
        $auth = new Auth($this->db, DEBUG);
        $rs = $auth->getAuthUser($this->db, array('email' => $data["username"]), 0);
        
        if(!empty($rs))
        {
            $newKey = $auth->resetKey($rs[0]["id"]);
            if(!$newKey)
            {
               
                // $response->body(json_encode(array('success' => false,  'errors' => $this->translation['userNotValidated'])));
                
                $this->jsonResponse(array("success" => false, "message" => $this->translation['userNotValidated']) ,405, $this->key);                    
                return;
            }
            $datas = array(
                        'subject'   => $this->translation["emailRecoverPasswordSubject"],
                        'from'      => array(
                                    'mail' =>   REFERER_EMAIL,
                                    'name' =>   REFERER_NAME
                                    ),
                        'to'        => array($data['username']),
                        'body'      => $this->getView('mailRecoverPassword', array('accessKey' => $newKey, 'base_url' => BASE_URL)),
                    );

            $this->sendMail($datas);
        }
        

       // $response->body(json_encode(array('success' => true,  'errors' => null, 'message' => $validation->translation['recoverPasswordMessageSent'])));
        $this->jsonResponse(array("success" => true, "message" => $this->translation['recoverPasswordMessageSent']) ,200, $this->key);            
        return;
    }
    
    public function register()
    {
        $data = $this->app->request()->params();
        $data = array_merge($data, $this->parameters);
        unset($data["password-recheck"]);
        
        $validator = array(
            'name'          =>  array('required'),
            'surname'       =>  array('required'),
            'email'         =>  array('required', 'email'),
            'password'      =>  array('required'),
            'birthplace'    =>  array('required'),
            'birthdate'     =>  array('required', 'date'),
            'sex'           =>  array('required', 'sex'),                
            'phone'         =>  array('required'),
            'address'       =>  array('required'),
            'address_civic' =>  array('required'),
            'zipcode'       =>  array('required'),
            'city'          =>  array('required'),
            'states'        =>  array('required'),
            'country'       =>  array('required'),
            'vatcode'       =>  array('required', 'vatcode'),
            'privacy'       =>  array('required'),
            'newsletter'    =>  array('required'),
        );
        
        /* yet to be implemented */
        if(isset($_FILES['profile_img']))
        {
            $data['profile_img'] = $_FILES['profile_img'];
            $validator['profile_img'] = array('required', 'image');
        }

        $validation = new errorHandling($this->translation, $validator);
        $validationRs = $validation->isValid($data);

        if(isset($validation->errors['critical'])){			
              /*  $response = $this->app->response();
                $response['Content-Type'] = 'application/json';
                $response['X-Powered-By'] = POWERED_BY;
                $response->status(400);

                $response->body(json_encode(array('success' => false,  'errors' => $validation->errors)));*/
                $this->jsonResponse(array("success" => false, "errors" => $validation->errors, "message"=>$this->translation['OperationFailed']) ,406, $this->key);                      
                return;
        }

        if(!empty($validation->errors)){			

                //$response->body(json_encode(array('success' => false,  'errors' => $validation->errors)));
                
                $this->jsonResponse(array("success" => false, "errors" => $validation->errors, "message"=>$this->translation['OperationFailed']) ,200, $this->key);                   
                return;
        }
        
        
        ///TODO UPLOAD IMAGE AND METHOD TO INSERT ACCOUNT AND USER
        if(isset($_FILES['profile_img']['name']))
        {
            $newImageName = date("Ymd").uniqid().urlencode($_FILES['profile_img']['name']);
            if($this->uploadImage($_FILES['profile_img'], IMAGE_ACCOUNT_DIR.$newImageName))
                    $data['profile_img'] = $newImageName;
        }
        $rs = $this->insertAccount($data);
        if($rs === ALREADY_IN){
            
            /*
            $response = $this->app->response();
            $response['Content-Type'] = 'application/json';
            $response['X-Powered-By'] = POWERED_BY;
            $response->status(406);

            $response->body(json_encode(array('success' => false,  'errors' => array('critical' => $validation->translation['userAlreadyExists']))));*/
            
            $this->jsonResponse(array("success" => false, "errors" => $validation->errors, "message"=>$this->translation['userAlreadyExists']) ,409, $this->key);               
            
        }
        elseif($rs === false){

/*
            $response->body(json_encode(array('success' => false,  'errors' => array('critical' => $validation->translation['insertFailed']))));
            */
                
                $this->jsonResponse(array("success" => false, "message"=>$this->translation['insertFailed']) ,400, $this->key);                
            
        }
        else
        {
            $emailData = array(
                    'subject'   => CONFIRM_EMAIL_SUBJECT,
                    'from'      => array(
                        'mail' =>   REFERER_EMAIL,
                        'name' =>   REFERER_NAME
                                ),
                    'to'        => array($data['email']),
                    'body'      => $this->getView('mailValidationAccount', array('encodedemail' => base64_encode($data['email']), 'base_url' => BASE_URL)),
                );
            
            $this->sendMail($emailData);
            
          //  $response->body(json_encode(array('success' => true,  'errors' => null, 'message' => str_replace(':name', $data['name'], $validation->translation['successMessage']))));
            
                $this->jsonResponse(array("success" => true, "message"=>str_replace(':name', $data['name'], $validation->translation['successMessage'])) ,200, $this->key);             
            return;                
        }
    }

    private function insertAccount($data)
    {
        $user = array(
            'email'     =>  $data['email'],
            'pass'      =>  hash(CRYPT_METHOD,$data['password']),
            'authlvl'   =>  1,
        );

        $userID = Auth::insertAuthUser($this->db, $user, 0);
        if($userID != false && $userID != ALREADY_IN)
        {
            $account = array(
                'authuser_id'       => $userID,
                'name'              => $data['name'],
                'surname'           => $data['surname'],
                'birthplace'        => $data['birthplace'],
                'birthdate'         => $data['birthdate'],
                'sex'               => $data['sex'],                    
                'phone'             => $data['phone'],
                'address'           => $data['address'],
                'address_civic'     => $data['address_civic'],
                'zipcode'           => $data['zipcode'],
                'city'              => $data['city'],
                'states'            => $data['states'],
                'country'           => $data['country'],
                'vatcode'           => $data['vatcode'],
                'privacy'           => $data['privacy'],
                'newsletter'        => $data['newsletter'],
                'active'            => 1,
            );

            if(isset($data['profile_img']))
                $account['profile_img'] = $data['profile_img'];

            $accountID = Account::insertAccount($this->db, $account, $userID);

            if($accountID != false && $accountID != ALREADY_IN)
            {    
                return true;
            }
            else
            {
                Auth::deleteAuthUser($this->db, array('id' => $userID), $userID);
                $userID = false;
            }
        }            

        return $userID;
    }
}
