        <footer>
            <section class="wrapper-full-width closure-footer">
                <div class="container">
                    <div class="col-6x">
                        Copyright Interjob 2016
                    </div>
                    <div class="col-6x">
                        <a href="/privacy">Privacy</a> - <a href="/credits">Credits</a>
                    </div>
                </div>
            </section>
        </footer>
                    
                    <!-- scripts -->
                    
        <script type="text/javascript" src="/resources/js/jquery-1.11.3.min.js"></script>
        <script type="text/javascript" src="/resources/js/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script type="text/javascript" src="/resources/js/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>                
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>   
        <script type="text/javascript" src="/resources/js/jquery.cookie.js"></script>	
        <script type="text/javascript" src="/resources/js/viewController.js"></script>	
        <script type="text/javascript" src="/resources/js/form.js"></script>	
        <script type="text/javascript" src="/resources/js/linkforall.js"></script>	
        <script type="text/javascript" src="/resources/js/search.js"></script>
        <script type="text/javascript" src="/resources/js/UpNCrop.js"></script>	
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHMX3Lxw92lUPuML4IWFQQEnPX9QQ3ID0&signed_in=true&libraries=places&callback=initAutocomplete" async defer></script>    
        <script type="text/javascript" src="/resources/js/jquery.endless-scroll.js"></script>
        <script type="text/javascript" src="/resources/js/:script"></script>	                
        
	</body>
</html>