<?php
require '../vendor/autoload.php';
require_once '../vendor/t4project/accessAuth/accessAuth.php'; 
require '../src/persister.php';
require '../app/models/baseModel.php';
require '../app/models/language.php';
require '../src/validator.php';

define("BASE_URL", "http://www.linkforall.it");
define("BASE_PATH", dirname(__FILE__));
define("DEBUG", true);
define("PAYPAL_SANDBOX", true);
define("PASSWORD_SALT", 'MySuperSecretPassword');
define("CRYPT_METHOD", 'sha512');
define("KEY_CRYPT_METHOD",'sha256');
define("ALREADY_IN", "Entry already in database");
define("NOT_CONFIRM", 102);
define("IMAGE_ACCOUNT_DIR", "images/account_profiles");
define("POWERED_BY", "Codelion");

//EMAIL
define("REFERER_EMAIL", "test@mailinator.com");
define("REFERER_NAME", "LinkForAll");


//COMPANIES

define("ORDERCOMPANY" , "ORDER BY profile_id DESC, company_name ASC");
define("MAXSEARCHRESULT" , "30");

//IMAGES MANAGE

define ("IMAGEQUALITY",90);
define ("TEMPIMGDIR","images/tmp/");

//PAYPAL

define ("PAYPAL_SECRET_KEY", "EOmgQeBwi_JH2TMKjOSZcDTk2rqWy7vB4n-WjB1Cyu3CcKXqQQ2OWxjkCjggbtwpnu69ORXVRa7UXrwo");
define ("PAYPAL_ID", "IDAcj6QOHzYclaEUxuofhSTmUEiipcmLnepABlb9qGzBizxiNKQPkgbtgozTGA1CwGWjaxw2YHPCpsoaXI");
define ("PAYPAL_CANCEL_URL", BASE_URL."/paypalCancel"); //CancelURL
define ("PAYPAL_RETURN_URL", BASE_URL."/paypalConfirm"); //ReturnURL
define ("PAYPAL_USE_PROXY", false);
define ("PAYPAL_VERSION", 84);
	
define ("PAYPAL_PROXY_HOST", '127.0.0.1'); //Proxy not used
define ("PAYPAL_PROXY_PORT", '808'); //Proxy not used
define ("PAYPAL_SBNCODE", "PP-ECWizard");


	
if(PAYPAL_SANDBOX)
{
    define ("PAYPAL_VENDOR_USERNAME", "xxxxx-facilitator_api1.gmail.com");
    define ("PAYPAL_VENDOR_PASSWORD", "xxxxx");
    define ("PAYPAL_VENDOR_SIGNATURE", "xxxxx");
    define ("PAYPAL_API_ENDPOINT", "https://api-3t.sandbox.paypal.com/nvp");
    define ("PAYPAL_URL", "https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=");
    define ("PAYPAL_DG_URL", "https://www.sandbox.paypal.com/incontext?token=");		
}
else
{
    define ("PAYPAL_VENDOR_USERNAME", "");
    define ("PAYPAL_VENDOR_PASSWORD", "");
    define ("PAYPAL_VENDOR_SIGNATURE", "");
    define ("PAYPAL_API_ENDPOINT", "https://api-3t.paypal.com/nvp");
    define ("PAYPAL_URL", "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=");
    define ("PAYPAL_DG_URL", "https://www.paypal.com/incontext?token=");    
}
//*DB config
if(DEBUG)
{
    define("SQL_HOST", 'localhost');
    define("SQL_DB", 'intjadmin');
    define("SQL_USER", 'mysql_intjadmin');
    define("SQL_PASS", 'intj274mtrx');
    define ('PAYPAL_MODE',"sandbox");

}
else
{
    define("SQL_HOST", 'localhost');
    define("SQL_DB", 'intjadmin');
    define("SQL_USER", 'mysql_intjadmin');
    define("SQL_PASS", 'intj274mtrx');
}

//*...and then all begin...
$app = new \Slim\Slim();
//$db = new MyPDO("pgsql:dbname=".SQL_DB.";host=".SQL_HOST.";username=".SQL_USER.";password=".SQL_PASS);
$db = new MyPDO("mysql:dbname=".SQL_DB.";host=".SQL_HOST, SQL_USER, SQL_PASS);

//require '../app/models/view.php'; //to be reincluded when all data and models are completed
require '../app/controllers/baseController.php';
require '../app/models/auth.php';
require '../app/models/account.php';
require '../app/models/log.php';
require '../app/models/company.php';
require '../app/models/profileType.php';
require '../app/models/companyTipology.php';
require '../app/routes.php';
