<div id="iscriversi" class="content">
   
    <h2><b></b>Registra la tua azienda tramite il <b>form qui sotto.</b></h2><br /><br />

    <div class="registrazione">
        <form class="registration" name="register_form" id="register_form" action="/newCompany" autocomplete="false" method="post" enctype="multipart/form-data">
                <fieldset class="clearfix">
                                                           
                    <div class="field left clearfix :error_company_name">
                        <label for="company_name">companyName*</label>
                        <input id="company_name" name="company_name" tabindex="1" maxlength="64" class="required" value="" type="text" />
                        <span class="errorLabel">:company_name</span>
                    </div>
                    <!--
                    <div class="field left clearfix :error_company_name">
                        <label for="company_slug">companySlug*</label>
                        <input id="company_slug" name="company_slug" tabindex="1" maxlength="64" class="required" value="" type="text" />
                        <span class="errorLabel">:company_slug</span>
                    </div>
                    -->
                    <!--
                    <div class="field :error_company_address">
                        <label for="company_address">company_address*</label>
                        <input id="company_address" name="company_address" tabindex="2" maxlength="64" class="required" type="text" value="" />
                        <span class="errorLabel">:company_address</span>
                    </div>
                    
                      <div class="field :error_company_city">
                        <label for="company_city">company_city*</label>
                        <input id="company_city" name="company_city" tabindex="2" maxlength="64" class="required" type="text" value="" />
                        <span class="errorLabel">:company_city</span>
                    </div>
                    
                      <div class="field :error_company_state">
                        <label for="company_city">company_state*</label>
                        <input id="company_state" name="company_state" tabindex="2" maxlength="64" class="required" type="text" value="" />
                        <span class="errorLabel">:company_state</span>
                    </div>
                    
                      <div class="field :error_company_country">
                        <label for="company_city">company_country*</label>
                        <input id="company_country" name="company_country" tabindex="2" maxlength="64" class="required" type="text" value="" />
                        <span class="errorLabel">:company_country</span>
                    </div> 
                     -->
                    
                     <div id="locationField">
                        <input id="autocomplete" autocomplete="off" placeholder="Enter your address"
                               onFocus="geolocate()" type="text"></input>
                      </div>

                      <table id="address">
                        <tr>
                          <td class="label">Street address</td>
                          <td class="slimField"><input class="field" name="company_number" id="street_number"
                                disabled="true"></input></td>
                          <td class="wideField" colspan="2"><input name="company_address" class="field" id="route"
                                disabled="true"></input></td>
                        </tr>
                        <tr>
                          <td class="label">City</td>
                          <td class="wideField" colspan="3"><input name="company_city" class="field" id="locality"
                                disabled="true"></input></td>
                        </tr>
                        <tr>
                          <td class="label">Provincia</td>
                          <td class="slimField"><input name="company_country" class="field"
                                id="administrative_area_level_2" disabled="true"></input></td>
                          <td class="label">Zip code</td>
                          <td class="wideField"><input name="company_cap" class="field" id="postal_code"
                                disabled="true"></input></td>
                        </tr>
                        <tr>
                          <td class="label">Country</td>
                          <td class="wideField" colspan="3"><input name="company_country" class="field"
                                id="country" disabled="true"></input></td>
                        </tr>
                      </table>
                    <input type="hidden" id="latgeo" name="latgeo" />
                    <input type="hidden" id="longgeo" name="longgeo" />


                    
                      <div class="field :error_company_url">
                        <label for="company_url">company_url*</label>
                        <input id="company_url" name="company_url" tabindex="2" maxlength="64" class="required" type="text" value="" />
                        <span class="errorLabel">:company_url</span>
                    </div>
                    
                      <div class="field :error_company_baseinfo">
                        <label for="company_baseinfo">company_baseinfo*</label>
                        <textarea id="company_baseinfo" name="company_baseinfo" tabindex="2" maxlength="64" class="required" type="text" > </textarea>                 
                        <span class="errorLabel">:company_baseinfo</span>
                    </div>
                    
                      <div class="field :error_company_description">
                        <label for="company_description">company_description*</label>
                        <textarea id="company_description" name="company_description" tabindex="2" maxlength="64" class="required" type="text" > </textarea>
                        <span class="errorLabel">:company_description</span>
                    </div>
                    
                      <div class="field :error_social_facebook_link">
                        <label for="social_facebook_link">social_facebook_link*</label>
                        <input id="social_facebook_link" name="social_facebook_link" tabindex="2" maxlength="64" class="required" type="text" value="" />
                        <span class="errorLabel">:social_facebook_link</span>
                    </div>                    
                    
                      <div class="field :error_social_linkedin_link">
                        <label for="social_linkedin_link">social_linkedin_link*</label>
                        <input id="social_linkedin_link" name="social_linkedin_link" tabindex="2" maxlength="64" class="required" type="text" value="" />
                        <span class="errorLabel">:social_linkedin_link</span>
                    </div>                    
                                    
                    <!-- new social -->
                    
                    <div class="field :error_social_500px_link">
                        <label for="social_500px_link">social_500px_link*</label>
                        <input id="social_500px_link" name="social_500px_link" tabindex="2" maxlength="64" class="" type="text" value="" />
                        <span class="errorLabel">:social_500px_link</span>
                    </div>                     
                    
                    <div class="field :error_social_flicker_link">
                        <label for="social_flicker_link">social_flicker_link*</label>
                        <input id="social_flicker_link" name="social_flicker_link" tabindex="2" maxlength="64" class="" type="text" value="" />
                        <span class="errorLabel">:social_flicker_link</span>
                    </div>                     
                    
                    <div class="field :error_social_google_plus_link">
                        <label for="social_google_plus_link">social_google_plus_link*</label>
                        <input id="social_google_plus_link" name="social_google_plus_link" tabindex="2" maxlength="64" class="" type="text" value="" />
                        <span class="errorLabel">:social_google_plus_link</span>
                    </div>                     
                    
                    <div class="field :error_social_instagram_link">
                        <label for="social_instagram_link">social_instagram_link*</label>
                        <input id="social_instagram_link" name="social_instagram_link" tabindex="2" maxlength="64" class="" type="text" value="" />
                        <span class="errorLabel">:social_instagram_link</span>
                    </div>                     
                    
                    <div class="field :error_social_pinterest_link">
                        <label for="social_pinterest_link">social_pinterest_link*</label>
                        <input id="social_pinterest_link" name="social_pinterest_link" tabindex="2" maxlength="64" class="" type="text" value="" />
                        <span class="errorLabel">:social_pinterest_link</span>
                    </div>                     
                    
                    <div class="field :error_social_soundcloud_link">
                        <label for="social_soundcloud_link">social_soundcloud_link*</label>
                        <input id="social_soundcloud_link" name="social_soundcloud_link" tabindex="2" maxlength="64" class="" type="text" value="" />
                        <span class="errorLabel">:social_soundcloud_link</span>
                    </div>                     
                    
                    <div class="field :error_social_tumblr_link">
                        <label for="social_tumblr_link">social_tumblr_link*</label>
                        <input id="social_tumblr_link" name="social_tumblr_link" tabindex="2" maxlength="64" class="" type="text" value="" />
                        <span class="errorLabel">:social_tumblr_link</span>
                    </div>                     
                    
                    <div class="field :error_social_twitter_link">
                        <label for="social_linkedin_link">social_twitter_link*</label>
                        <input id="social_twitter_link" name="social_twitter_link" tabindex="2" maxlength="64" class="" type="text" value="" />
                        <span class="errorLabel">:social_twitter_link</span>
                    </div>                     
                    
                    <div class="field :error_social_vimeo_link">
                        <label for="social_vimeo_link">social_vimeo_link*</label>
                        <input id="social_vimeo_link" name="social_vimeo_link" tabindex="2" maxlength="64" class="" type="text" value="" />
                        <span class="errorLabel">:social_vimeo_link</span>
                    </div>                     
                    
                    <div class="field :error_social_youtube_link">
                        <label for="social_youtube_link">social_youtube_link*</label>
                        <input id="social_youtube_link" name="social_youtube_link" tabindex="2" maxlength="64" class="" type="text" value="" />
                        <span class="errorLabel">:social_youtube_link</span>
                    </div>                     
                    
                    
                    <!-- end new social -->                    
                    
                      <div class="field :error_company_keywords">
                        <label for="company_keywords">company_keywords* inserirle divise da un ; </label>
                        <input id="company_keywords" name="company_keywords" tabindex="2" maxlength="64" class="required" type="text" value="" />
                        <span class="errorLabel">:company_keywords</span>
                    </div>                                                             
                    
                    <!--
                    <div class="field :error_photo">
                        <label for="day">Foto</label>
                        <input type="file" name="photo" tabindex="6" class="required" id="photo" value="">
                        <span class="errorLabel">:photo</span>
                    </div>
                    -->
                </fieldset>
            
            
            
            <!--
                <div class="laboratori">
                    <p>Seleziona i tuoi interessi per i workshop pomeridiani:</p>
                    <div class="labField">
                        <div class="field">
                            <input id="ios" name="ios" class="preferences" tabindex="7" type="checkbox"  />
                            <label class="preferencies_label">iOS</label>
                        </div>
                        <div class="field">
                            <input id="android" name="android" class="preferences" tabindex="8" type="checkbox"  />
                            <label class="preferencies_label">Android</label>
                        </div>
                        <div class="field">
                            <input id="windows" name="windows" class="preferences" tabindex="9" type="checkbox"  />
                            <label class="preferencies_label">Windows Phone</label>
                        </div>
                        <div class="field">
                            <input id="bb" name="bb" class="preferences" tabindex="10" type="checkbox"  />
                            <label class="preferencies_label">BB</label>
                        </div>
                    </div>
                </div>-->

            
                <div class="field privacy :error_privacy">
                    <label class="privacy_label">Ho letto e accetto <a href="/privacy" target="_blank">l'informativa</a> sulla Privacy*</label>
                    <input data-rules="privacy" id="privacy" name="privacy" class="privacy_input required" tabindex="11" type="checkbox"  />
                    <span class="errorLabel">:privacy</span>
                </div>
                <div class="field service"></div>
                <p style="font-size:13px;">Tutti i campi con * sono obbligatori.</p>
                <a href="#" class="button registrar">Iscrivi l'azienda</a>
                <div class="loader"></div>
        </form>

        <!-- CONFERMA AVVENUTA ISCRIZIONE -->
        <h3 class="successful">:successMessage</h3>
        <!-- La tua registrazione è andata a buon fine. Tra poco riceverai una mail di conferma contenente le istruzioni. -->

        <!-- ERRORE REGISTRAZINOE -->
        <h3 class="mistake">:failureMessage</h3>
    </div>
    <div class="clear"></div>
</div>

