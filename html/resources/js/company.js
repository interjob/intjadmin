
// company registration

Form.checkInput = function(form){
	//JS validation
	$(form).find('input,textarea,checkbox,radio,select').each(function(key,element) {
		if($(element).hasClass('required')){
			switch($(element).attr('type')){
				case 'checkbox':
							if(!$(element).is(':checked'))
					        	$(element).parent('div').addClass('error');
							break;
				case 'radio':
							if(!$(element).is(':checked'))
					        	$(element).parent('div').addClass('error');
                                                    
							break;
				case 'email':
                                                        if (!Form.isValidEmailAddress($(element).val()))                                                                  
                                                             $(element).parent('div').addClass('error');  

							break;
				case 'password':
							if($("#password").val()!=$("#password-recheck").val()){
                                                            $(element).parent('div').addClass('error');                                                            
                                                          //  $("#password").addClass('error');
                                                          //  $("#password-recheck").addClass('error');
                                                        }
                                                        break;
				case 'file':
							/*if(!$.browser.msie){
								if(!Form.validateImg(element.files[0])){
									$(element).val('');
						        	$(element).parent('div').addClass('error');
						        }
						    }*/
							break;
				default:
							if($(element).val()=='')
								$(element).parent('div').addClass('error');
							break;

			}
		}

    });

};


Form.submitNoErrors = function(data, textStatus, jqXHR){
        console.log(textStatus);
                console.log(data);
      /*  switch(textStatus){
            case :
        }*/
    
	if(!data.success){
    	$.each(data.errors, function(key, value){
    		$('#' + key).parent('.field').addClass('error');
    		$('<span class="errorLabel">' + value + '</span>').insertAfter('#' + key)
    	});
    	Form.attachEventOne(jQuery('.registrar'), 'click', 'Form.submit($(this).closest(\'form\'));');
    	$('.loader').delay(200).fadeOut('fast');
    	$('.registrar').delay(400).slideDown('fast');
    }
    else
    {
    	Form.attachEventOne(jQuery('.registrar'), 'click', '');
    	Form.successRegister(data.message);
    }
};


Form.successRegister = function(message){
	$('#register_form').slideUp('fast');
	$('.registrazione .successful').text(message);
	$('.registrazione .successful').slideDown('fast');
        
        setTimeout(function(){
                 Form.redirect("/accountDashboard/"+accessKey);
        }, 3000);

};

/*
Form.errorRegister = function(data){
	$('#register_form').slideUp('fast');
	$('.registrazione .mistake').show('fast');
        $(".mistake").html($(".mistake").html()+" "+data.errors.critical);
};
                        */

Form.dataAcquisionCompany = function (){
    var input = {}; 
    input.params={};    
    if ($("#companySlug").length>0){

            input.params.company_name =  $('#company_name').val();
            input.params.company_subtitle =  $('#company_subtitle').val();
            input.params.company_url =  $('#company_url').val();
            input.params.company_baseinfo =  $('#company_baseinfo').val();  
            input.params.company_description =  $('#company_description').val();
            input.params.company_services =  $('#company_services').val();
            input.params.social_facebook_link =  $('#social_facebook_link').val();
            input.params.social_linkedin_link =  $('#social_linkedin_link').val();
            //new social
            input.params.social_500px_link =  $('#social_500px_link').val();
            input.params.social_flicker_link =  $('#social_flicker_link').val();
            input.params.social_google_plus_link =  $('#social_google_plus_link').val();
            input.params.social_instagram_link =  $('#social_instagram_link').val();
            input.params.social_pinterest_link =  $('#social_pinterest_link').val();
            input.params.social_soundcloud_link =  $('#social_soundcloud_link').val();
            input.params.social_tumblr_link =  $('#social_tumblr_link').val();
            input.params.social_twitter_link =  $('#social_twitter_link').val();
            input.params.social_vimeo_link =  $('#social_vimeo_link').val();
            input.params.social_youtube_link =  $('#social_youtube_link').val();            
            //end new social
            input.params.company_keywords =  $('#company_keywords').val();            
            input.params.company_slug =  $('#companySlug').val();            
        
        }else{

            input.params.company_name =  $('#company_name').val();
            input.params.company_address =  $('#route').val()+ ", "+$('#street_number').val();
            input.params.company_city =  $('#locality').val();
            input.params.company_state =  $('#administrative_area_level_2').val();          //provincia
            input.params.company_cap =  $('#postal_code').val();
            input.params.company_country =  $('#country').val();                
            input.params.latgeo =  $('#latgeo').val();
            input.params.longgeo =  $('#longgeo').val();

            input.params.categories= [1];

            input.params.company_url =  $('#company_url').val();
            input.params.company_baseinfo =  $('#company_baseinfo').val();
            input.params.company_description =  $('#company_description').val();
            input.params.social_facebook_link =  $('#social_facebook_link').val();
            input.params.social_linkedin_link =  $('#social_linkedin_link').val();
            //new social
            input.params.social_500px_link =  $('#social_500px_link').val();
            input.params.social_flicker_link =  $('#social_flicker_link').val();
            input.params.social_google_plus_link =  $('#social_google_plus_link').val();
            input.params.social_instagram_link =  $('#social_instagram_link').val();
            input.params.social_pinterest_link =  $('#social_pinterest_link').val();
            input.params.social_soundcloud_link =  $('#social_soundcloud_link').val();
            input.params.social_tumblr_link =  $('#social_tumblr_link').val();
            input.params.social_twitter_link =  $('#social_twitter_link').val();
            input.params.social_vimeo_link =  $('#social_vimeo_link').val();
            input.params.social_youtube_link =  $('#social_youtube_link').val();            
            //end new social            
            input.params.company_keywords =  $('#company_keywords').val();
            input.params.privacy =  $('#privacy').val();            
        }
    return input;
}

Form.submitAJAX = function(form) {
	/*if($.browser.msie){
		$('#register_form').attr('action', '/success');
		$('#register_form').submit();
	}
	else{*/
            
           /* $("#register_form").submit();*/
            
            
               // var formInput = new FormData($(form)[0]);         //get all form data
                             
                             
                             
                             
               var input = Form.dataAcquisionCompany();          
               
                input.params.accessKey = accessKey;
                input=JSON.stringify(input);


		var defaultData ={
			url 				: $(form).attr('action'),
			methodForm 			: 'POST',
			beforeSendHandler 	: false,
		/*	errorHandler 		: 'Form.errorRegister(data)',    */         
			errorHandler 		: 'Form.errorHandler',             
			successHandler 		: 'Form.submitNoErrors(data,textStatus,jqXHR)',
			completeHandler		: false,
			formData			: input,
			dataType			: "json",
			cache				: false,
			contentType			: false,
			processData			: false,
			crossDomain			: false
		};

		Form.ajaxCall(defaultData);            
            
            
            return false;
		/*
                var formInput = new FormData($(form)[0]);                
		var defaultData ={
			url 				: $(form).attr('action'),
			methodForm 			: 'POST',
			beforeSendHandler 	: false,
			errorHandler 		: 'Form.errorRegister',
			successHandler 		: 'Form.submitNoErrors(data, textStatus, jqXHR)',
			completeHandler		: false,
			formData			: formInput,
			dataType			: "json",
			cache				: false,
			contentType			: false,
			processData			: false,
			crossDomain			: false
		};

		Form.ajaxCall(defaultData);*/
	//}

};



Form.submit = function(form){
	$('.errorLabel').remove();
	$('.field').removeClass('error');

	Form.checkInput(form);

	//Form.AJAXValidation(form);

	if($('.error').length == 0){
		$('.registrar').slideUp('fast');
		$('.loader').fadeIn('fast');
		Form.attachEventOne(jQuery('.registrar'), 'click', '');
		Form.submitAJAX(form);
	}
	else
            Form.attachEventOne(jQuery('.registrar'), 'click', 'Form.submit($(this).closest(\'form\'));');
};



jQuery(document).on('ready', function(){            
	Form.attachEventOne(jQuery('.registrar'), 'click', 'Form.submit($(this).closest(\'form\'));');
        Form.attachEventOne(jQuery(".removeCompanyClick"), "click", "Form.RemoveCompany($(this));");
        Form.attachEventOne(jQuery(".buttonRemoveAddress"), "click", "Form.RemoveAddress($(this).parent());");
        Form.attachEventOne(jQuery(".addAddress"), "click", 'Form.AddAddress($(this).closest(\'form\'));');
});



//company dashboard




Form.RemoveCompany = function(target){
          
                var input = {};
                input.params={};
                input.params.company_slug =  $(target).attr("data-attr");                
                input.params.accessKey = accessKey;
                
                var targethide=  $(target).attr("data-attr").toString();
                           //$("#"+target).remove();
                    
                input=JSON.stringify(input);

		var defaultData ={
			url 				: "/removeCompany",
			methodForm 			: 'POST',
			beforeSendHandler 	: false,
		/*	errorHandler 		: 'alert("error removing company")',   */          
			errorHandler 		: 'Form.errorHandler',                
			successHandler 		: 'Form.submitNoErrors(data,textStatus,jqXHR)',                        
			completeHandler		: false,
			formData			: input,
			dataType			: "json",
			cache				: false,
			contentType			: false,
			processData			: false,
			crossDomain			: false
		};

		Form.ajaxCall(defaultData); 
                
              $("#"+targethide).remove();
}



Form.RemoveAddress = function(element){
    
    var address = $(element).find(".idAddressRemove").val();    
    var traslatedMessageRemove = $(".traslatedMessageRemove").attr("title");
    $("#getListAddresses").attr("data-sel", address);
    var r = confirm(traslatedMessageRemove);
    if (r == true) {
        
                var input = {};
                input.params={};
                input.params.idAddressRemove = address;                
                input.params.accessKey = accessKey;
                
    
                           //$("#"+target).remove();
                    
                input=JSON.stringify(input);

		var defaultData ={
			url 				: "/removeAdressCompany",
			methodForm 			: 'POST',
			beforeSendHandler 	: false,
		/*	errorHandler 		: 'alert("error removing company")',   */          
			errorHandler 		: 'Form.errorHandler',                
			successHandler 		: 'Form.submitAddressRemoved(data,textStatus,jqXHR)',                        
			completeHandler		: false,
			formData			: input,
			dataType			: "json",
			cache				: false,
			contentType			: false,
			processData			: false,
			crossDomain			: false
		};

		Form.ajaxCall(defaultData);         
                
                
        
    } else {
        Form.attachEventOne(jQuery(".buttonRemoveAddress"), "click", "Form.RemoveAddress($(this).parent());");
    }
        
};


Form.submitAddressRemoved = function (data,textStatus,jqXHR){
    
        var selector = $("#getListAddresses").attr("data-sel");
        var targethide=  $('#addressID_' + selector).slideUp("fast",function(element){
              $(element).remove();
          });        
          
          
};



Form.AddAddress = function (form){
    
    var input = {}; 
    input.params={};    
    

            input.params.company_slug =  $('#company_slug').val();
            input.params.company_address =  $('#route').val()+ ", "+$('#street_number').val();
            input.params.company_city =  $('#locality').val();
            input.params.company_state =  $('#administrative_area_level_2').val();          //provincia
            input.params.company_cap =  $('#postal_code').val();
            input.params.company_country =  $('#country').val();                
            input.params.latgeo =  $('#latgeo').val();
            input.params.longgeo =  $('#longgeo').val();
            input.params.accessKey = accessKey;

            input=JSON.stringify(input);

		var defaultData ={
			url 				: "/addAdressCompany",
			methodForm 			: 'POST',
			beforeSendHandler 	: false,
		/*	errorHandler 		: 'alert("error removing company")',   */          
			errorHandler 		: 'Form.errorHandler',                
			successHandler 		: 'Form.AddAddressSuccess(data,textStatus,jqXHR)',                        
			completeHandler		: false,
			formData			: input,
			dataType			: "json",
			cache				: false,
			contentType			: false,
			processData			: false,
			crossDomain			: false
		};

		Form.ajaxCall(defaultData); 
                        
}


Form.AddAddressSuccess = function (data,textStatus,jqXHR){
    
    if (data.success){
        Form.successRegister(data.message);
        /*
        setTimeout(function(){
                 Form.redirect("/accountDashboard/"+accessKey);
        }, 3000);*/
                
    }else{
        $("errorLabel").text(data.message).show();
    }
    

       //     Form.attachEventOne(jQuery(".addAddress"), "click", 'Form.AddAddress($(this).closest(\'form\'));');
            
};
