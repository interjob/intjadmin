<?php
class baseModel
{
	static public function insert($db, $params, $table, $authuserID)
	{
		//Set createdat parameter
		$params["createdat"] = date("Y-m-d H:i:s");

		$setColumns = implode(", ", array_keys($params));
		$queryParams = $db->prepareParameters($params);
		$setValues = implode(", ", array_keys($queryParams));
		//$setValues = implode(", ", $params);

		$query = "INSERT INTO $table ( $setColumns ) VALUES ($setValues)";
                
		$return = MyPDO::myExec($db, $query, $queryParams, $authuserID);

                if($return)
                    return $db->lastInsertId();
                else
                    return false;
	}

	static public function insertIfNotExist($db, $params, $table, $returning, $authuserID)
	{
		//Set createdat parameter
		$conditions = $db->prepareUpdateConditionsAndSets($params, true);
		$params["createdat"] = date("Y-m-d H:i:s");

		$setColumns = implode(", ", array_keys($params));
		$queryParams = $db->prepareParameters($params);
		$setValues = implode(", ", array_keys($queryParams));

		$query = "WITH s AS (
    				SELECT id, $setColumns
    				FROM $table
    				WHERE $conditions
				), i AS (
    				INSERT INTO $table ( $setColumns )
    				SELECT $setValues
    				WHERE NOT EXISTS (SELECT 1 FROM s)
    				RETURNING $returning
				)
				SELECT $returning
				 FROM i
				 UNION ALL
				 SELECT $returning
				 FROM s";

		$return = MyPDO::myExec($db, $query, $queryParams, $authuserID);

		if(!isset($return[$returning]))
			return $return[0][$returning];

		return $return[$returning];
	}

	static public function update($db, $params, $table, $conditions, $authuserID)
	{
		//Set createdat parameter
		$params["updatedat"] = date("Y-m-d H:i:s");

		$sets = $db->prepareUpdateConditionsAndSets($params);
		$queryParams = $db->prepareParameters($params);
		$conditions = $db->prepareUpdateConditionsAndSets($conditions, true);

		$query = "UPDATE $table SET $sets WHERE $conditions";
		$return = MyPDO::myExec($db, $query, $queryParams, $authuserID);
                //var_dump($return);
                if($return > 0)
                    return true;
                else
                    return false;
	}

	static public function delete($db, $params, $table, $authuserID)
	{
		$paramsQuery = array();
		$where = '';
		if(count($params) > 0)
		{
			foreach ($params as $key => $value) {
				if(strlen($where) == 0)
					$where = ' WHERE ';
				else
					$where.= ' AND ';
				$where.= " $key = :$key ";
				$paramsQuery[':'.$key] = $value;
			}
		}
		$query = "DELETE FROM $table $where";

		return MyPDO::myExec($db, $query, $paramsQuery, $authuserID);
	}

	static public function get($db, $params, $table, $authuserID, $returning = '*')
	{
		$paramsQuery = array();
		$where = '';
		if(count($params) > 0)
		{
			foreach ($params as $key => $value) {
				if(strlen($where) == 0)
					$where = ' WHERE ';
				else
					$where.= ' AND ';
				$where.= " $key = :$key ";
				$paramsQuery[':'.$key] = $value;
			}
		}
		$query = "SELECT $returning FROM $table $where";  
                
		return MyPDO::myExec($db, $query, $paramsQuery, $authuserID, false);
	}

	static public function getOrdered($db, $params, $table, $authuserID, $order="ORDER BY id ASC", $returning = '*')
	{
		$paramsQuery = array();
		$where = '';
		if(count($params) > 0)
		{
			foreach ($params as $key => $value) {
				if(strlen($where) == 0)
					$where = ' WHERE ';
				else
					$where.= ' AND ';
				$where.= " $key = :$key ";
				$paramsQuery[':'.$key] = $value;
			}
		}
		$query = "SELECT $returning FROM $table $where $order";  
                
		return MyPDO::myExec($db, $query, $paramsQuery, $authuserID, false);
	}
        
        //*public static method to get entity translation
	/**
		this method return entity list translated
		$db : PDO resource to connect and retrieve data
		$table : name of db table for entity list
                $params : parameter for entity table selection (WHERE)
                $lang   : codelanguage to identify current language in i18n table
                $joinTableKey : table key column to use to identify correct translation
		$userID : it's identifier of user that do this action
		return list of entity with translation in one language
	*/
	static public function getTranslatedValue($db, $table, $params, $lang, $joinTableKey, $authuserID)
	{
		$paramsQuery = array();
		$where = '';
		if(count($params) > 0)
		{
			foreach ($params as $key => $value) {
				if(strlen($where) == 0)
					$where = ' WHERE ';
				else
					$where.= ' AND ';
				$where.= " $key = :$key ";
				$paramsQuery[':'.$key] = $value;
			}
                        $where.=" language_code = :language_code";
                        $paramsQuery[':language_code'] = $lang;
		}
                
                $join = " LEFT JOIN languages_i18n AS lang ON lang.code_key = ".$joinTableKey;
                
		$query = "SELECT * FROM $table $join $where"; 
                
                //echo $query;die();
                
		return MyPDO::myExec($db, $query, $paramsQuery, $authuserID, false);
	}
}