<?php
$translation = array(
    
    //Validation
	'Mandatory' 			=> 'Campo Obbligatorio',
	'NotNumber' 			=> 'Valore non numerico',
	'NotValidDate' 			=> 'Data non valida',
	'NotValidEmail' 		=> 'Email non valida',
	'NotValidFileFormat'            => 'Formato file non supportato',
	'userAlreadyExists'             => 'Utente già presente',
        'RegisterParameter'             => 'Errore nell\'inserimento, riprovare più tardi.',
        'NotValidVatCodeFormat'         => 'Codice fiscale non valido.',
        'failureMessage'                => 'Ci sono stati errori durante l\'elaborazione della richiesta, riprovare più tardi. ',
        'NotValidSex'                   => 'Sesso non specificato',
        'GeoError'                      => 'Geolocalizzazione fallita, riprovare',
    
    //Returning Status
        'insertFailed'                  => 'Inserimento fallito.',
        'userNotValidated'              => 'Non è possibile cambiare la password ad un utente non validato.',
        'recoverPasswordMessageSent'    => 'Un messaggio verrà inviato all\'indirizzo indicato nel caso l\'utente sia valido, il messaggio conterrà le indicazioni per resettare la password.',
        'accountAlreadyVerified'        => 'Account già verificato',
        'accountCorrectlyVerified'      => 'Account verificato correttamente',
        'accountNotVerified'            => 'Account non verificato',
        'passwordCorrectlySet'          => 'Password cambiata correttamente',
        'passwordSetError'              => 'Errore nel cambiamento della password',
        'loginFailed'                    => 'Utente o password errate',
        'loginSuccess'                    => 'Login avvenuto con successo',
        'OperationSuccess'             => 'Operazione avvenuta con successo',
        'OperationFailed'              => 'Operazione fallita',
        'LogoutSuccess'                => 'Logout avvenuto con successo',
        'LogoutFailed'                 => 'Logout Fallito',
    
    
    //Head content
        'titleHome'                     => 'Home',
        'titleRegistration'             => 'Registrazione',
        'titleLogin'                    => 'Login',
        'titleLostPassword'             => 'Password Dimenticata',
        'titleSetPassword'              => 'Inserisci nuova Password',
        'titleAccountValidation'        => 'Conferma Account',
    
    
        'placeToSearch'                => 'località...',
        'categoriesSearch'             => 'categoria...',
        'keywordsSearch'               => 'parola chiave...',
    
        'viewCompanyContent'            => 'visualizza profilo',
        'noResult'                      => 'Nessuna Azienda trovata',
    
    //Email Content
        'emailRecoverPasswordSubject'   => 'Cambia la tua password',
    
	'info1'					=> 'INFORMATIVA AI SENSI DEL D.LGS. 30 GIUGNO 2003, N. 196 CODICE IN MATERIA DI PROTEZIONE DEI DATI PERSONALI',
	'info2'					=> 'La legge 196/03 disciplina il trattamento dei dati personali; la legge prevede che al soggetto interessato vengano fornite le informazioni di seguito elencate.
I dati raccolti saranno trattati in modo lecito, secondo correttezza e con la massima riservatezza, saranno organizzati e conservati in archivi informativi e/o cartacei e saranno utilizzati per l’assolvimento degli obblighi previsti dalla legge, con particolare riguardo a quelli contabili e fiscali. I dati trattati non saranno comunicati a terzi estranei. In occasione delle operazioni di trattamento dei Vostri dati personali, a seguito di Vostra eventuale segnalazione o di elaborazione dei documenti da Voi consegnati, T4 Project srl può venire a conoscenza di dati che la legge definisce “sensibili” (dati personali idonei a rilevare l’origine razziale ed etnica, le convinzioni religiose, filosofiche o di altro genere, le opinioni politiche, l’adesione a partiti, sindacati, associazioni od organizzazioni a carattere religioso, filosofico, politico o sindacale, nonché i dati personali idonei a rilevare lo stato di salute e la vita sessuale del cliente). Il trattamento di documenti relativi ad eventuali spese mediche, così come la scelta di destinazione dell’8 per mille, ci mette ad esempio a conoscenza di un dato “sensibile” per il quale la legge prevede la possibilità di trattamento soltanto con il consenso scritto dell’interessato. Vi preghiamo, quindi, di voler sottoscrivere la presente come ricevuta dell’informativa avuta e come consenso scritto al trattamento dei Vostri dati ed in particolare di quelli sensibili, pena l’impossibilità di procedere al trattamento dei dati.',
	'info3'					=> 'CONSENSO AI SENSI DEL D.LGS. 30 GIUGNO 2003, N. 196 CODICE IN MATERIA DI PROTEZIONE DEI DATI PERSONALI',
	'info4'					=> 'La sottoscritta T4 Project srl in persona del legale rappresentante, in qualità di conferente l’incarico, dichiara di aver ricevuto completa informativa ai sensi dell’art. 13 della Legge 196/03 ed esprime il proprio consenso al trattamento dei dati che riguardano l’adempimento degli obblighi legali, con particolare riguardo a quelli contabili e fiscali e con particolare riferimento ai cosiddetti dati “sensibili”.',
	'successMessage'		=> 'Grazie :name, abbiamo preso in carico la tua registrazione. A breve riceverai una mail contenente tutte le informazioni necessarie per l\'evento.',
	'successContactMessage'	=> 'Grazie :name, risponderemo appena possibile alle tue richieste.',
	'ios'					=> 'iOS',
	'android'				=> 'Android',
	'windows'				=> 'Windows Phone',
	'bb'					=> 'BlackBerry',
	'export_success'		=> 'Esportazione eseguita correttamente!',
	'export_failure'		=> 'Esportazione non portata a termine, contattare l\'amministratore di sistema',
    
    
     //Privacy static page  
        'privacy_text'                  => "testo lungo privacy",
    
    //Companies
        'upgrade_profile'               => "Upgrade account",
        'edit_profile'                  => "modifica dati aziendali",
        'edit_addresses'                  => "modifica indirizzi",
        'edit_promos'                  => "modifica promos",
        'edit_jobs'                  => "modifica jobs",
        'edit_swaps'                  => "modifica swaps",
        'CompanyNotAvailable'           => "Azienda non più disponibile",
        'NoResultFound'                 => "Nessun azienda trovata",
        'successMessageUpdateCompany'  => "L'azienda :name è stata modificata con successo.",
        'editAddress'  => "Modifica indirizzo",
        'removeAddress'  => "Elimina indirizzo",
    
        'addressLabel'  => "Indirizzo: ",
        'cityLabel'  => "Città: ",
        'capLabel'  => "Cap: ",
        'countryLabel'  => "Provincia: ",
        'traslatedMessageRemove'  => "Vuoi veramente eliminare l'indirizzo?",
        'NewAddress'  => "Inserisci indirizzo",
       
);