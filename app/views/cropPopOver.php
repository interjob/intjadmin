<div class="overlay"></div>
<div class="popoverImage">
	<div class="dropzone"></div>
	<button class="close-btn">Close</button>
	<div class="crop">
		<img src="" id="cropbox" />
		
		<input type="hidden" id="x" name="x" />
		<input type="hidden" id="y" name="y" />
		<input type="hidden" id="w" name="w" />
		<input type="hidden" id="h" name="h" />
		<input type="hidden" id="imageName" name="imageName" />
		<input type="button" value="Crop Image" class="btn btn-large btn-inverse" id="cropButton" />
		
	</div>
</div>

