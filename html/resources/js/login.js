Form.submitNoErrors = function(data, textStatus, jqXHR){
    console.log(data);
    console.log(textStatus);
    console.log(jqXHR.status);
    $(".loginMessage").html(data.message);
    $(".loginMessage").show();      
    if(jqXHR.status===200){
       //redirect
       console.log(data.accessKey);
            $.cookie("accessKey", data.accessKey,{ expires : 1 });           
            Form.attachEventOne(jQuery('.submitlogin'), 'click', '');        
            setTimeout("Form.redirect('/')",1200);               
    }
    else
    {     
    	Form.attachEventOne(jQuery('.registrar'), 'click', 'Form.submit($(this).closest(\'form\'));');                    
    }
};


Form.submitComplete = function(data, textStatus, jqXHR){  
    Form.attachEventOne(jQuery('.registrar'), 'click', 'Form.submit($(this).closest(\'form\'));');                    
};

Form.submitAJAX = function(form) {

              /*
                var formInput = new FormData();
          
                formInput.append( 'username', $('#username').val());                
                formInput.append( 'password', $('#password').val());  */
            
                var input = {};
                input.params={};
                input.params.username =  $('#username').val();
                input.params.password =  $('#password').val();
                input=JSON.stringify(input);
                
		var defaultData ={
			url 				: $(form).attr('action'),
			methodForm 			: 'POST',
			beforeSendHandler 	: false,
	/*		errorHandler 		: 'Form.submitNoErrors(data,textStatus,jqXHR)',             */
			errorHandler 		: 'Form.errorHandler',        
			successHandler 		: 'Form.submitNoErrors(data,textStatus,jqXHR)',
			completeHandler		: 'Form.submitComplete(data,TextStatus,jqXHR)',
			formData			: input,
			dataType			: "json",
			cache				: false,
			contentType			: false,
			processData			: false,
			crossDomain			: false
		};

		Form.ajaxCall(defaultData);                        
            
            return false;

};



Form.submit = function(form){
	$('.errorLabel').remove();
	$('.field').removeClass('error');
        Form.submitAJAX(form);
};

jQuery(document).on('ready', function(){ 
    	Form.attachEventOne(jQuery('.submitlogin'), 'click', 'Form.submit($(this).closest(\'form\'));');
});