        <header>
            <section class="wrapper-full-width top-header">
                <div class="container">
                    <div class="row">
                        <div class="col-12x">
                            <div class="social-icon-container">
                                <i class="fa fa-facebook-f fa-lg"></i>
                                <i class="fa fa-twitter fa-lg"></i>
                                <i class="fa fa-instagram fa-lg"></i>
                            </div>
                            <div id="accountRegistration" class="right-floated-menu">
                                <a href="/login">ACCEDI</a> - <a href="/registration">registrati</a> - 
                            </div>
                            <div id="accountProfileDashboard" class="right-floated-menu">
                                <a class="accessVal" href="/accountDashboard">Account</a> - <a href="#" id="logoutClick">Logout</a>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
