<?php

class CompanyTipology extends baseModel
{
	//* public static method to insert company_tipology
	/**
		this method insert new company_tipology with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
					'code_profile'              => 'free',
                                        'title'                     => 'profiles_types_title_free',
                                        'description'               => 'profiles_types_description_free',                                        
                                        'icon'                      => 'path/to/image',                                        
					'createdat'                 => '19900219 14:22:45',
					'updatedat'                 => '19900219 14:22:45',
                                        'active'                    => 1,
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function insertCompanyTipology($db, $params, $userID)
	{
		$rs = self::get($db, $params, 'companies_typologies', $userID);

		if(isset($rs[0]))
			return ALREADY_IN;
		else
			return parent::insert($db, $params, 'companies_tipologies', 'id', $userID);
	}

	//* public static method to update company_tipology
	/**
		this method update existing company_tipology with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
					'code_profile'              => 'free',
                                        'title'                     => 'profiles_types_title_free',
                                        'description'               => 'profiles_types_description_free',                                        
                                        'icon'                      => 'path/to/image',                                        
					'createdat'                 => '19900219 14:22:45',
					'updatedat'                 => '19900219 14:22:45',
                                        'active'                    => 1,
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function updateCompanyTipology($db, $params, $conditions, $userID)
	{
		return parent::update($db, $params, 'companies_tipologies', 'id', $conditions, $userID);
	}

	//*public static method to delete company_tipology
	/**
		this method delete one entity row
		$db : PDO resource to connect and retrieve data
		$id : mandatory for find what to delete
		$userID : it's identifier of user that do this action
		return true or false if the operation do its job or not
	*/
	static public function deleteCompanyTipology($db, $params = array(), $userID)
	{
		return parent::delete($db, $params, 'companies_tipologies', $userID);
	}

	//*public static method to get company_tipology
	/**
		this method return entity list or if id parameter is not null get details of one company_tipology entity
		$db : PDO resource to connect and retrieve data
		$id : optional parameter, if it is set get single company_tipology entity
		$userID : it's identifier of user that do this action
		return list of company_tipology or single company_tipology
	*/
	static public function getCompanyTipology($db, $params = array(), $userID)
	{
		return parent::get($db, $params, 'companies_typologies', $userID);
	}

	//*public static method to get translated company_tipology
	/**
		this method return entity list or if id parameter is not null get details of one company_tipology entity
		$db : PDO resource to connect and retrieve data
		$table : name of db table for entity list
                $params : parameter for entity table selection (WHERE)
                $lang   : codelanguage to identify current language in i18n table
                $joinTableKey : table key column to use to identify correct translation
		$userID : it's identifier of user that do this action
		return list of entity with translation in one language
	*/
        static public function getTranslatedompanyTipology($db, $params, $lang, $userID)
        {
                return parent::getTranslatedValue($db, 'companies_typologies', $params, $lang, 'title', $userID);
        }
}
