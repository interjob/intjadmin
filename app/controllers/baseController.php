<?php
abstract class BaseController {

	protected $app;
	protected $db;
	public $language;
        protected $key = false;
        protected $parameters = array();
        public $translation;

	function __construct($app, $db = null, $lang = 'it'){
		$this->app = $app;
		$this->db = $db;
		$this->language = new Language($lang);
                $this->translation = $this->language->getTranslation();
	}

	public function getViewBaseData($view, $data){
		return array(
			'head' 		=> array('title' => 'LinkForAll', 'style' => 'style'),
			'menu'		=> array(),
			'header' 	=> array(),
			$view 		=> $data,
			'footer' 	=> array(),
		);
	}

	public function render($view, $data, $layoutSchema = array()){
		if(empty($layoutSchema))
			$pageData = $this->getViewBaseData($view, $data);
		else
			$pageData = $layoutSchema;

		foreach($pageData as $schema => $value):
			echo $this->getView($schema, $value);
		endforeach;
    }

	public function getView($view, $data = array()){
	    $tpl = file_get_contents("../app/views/{$view}.php", TRUE);
	    foreach ($data as $key => $value):
	        $tpl = str_replace(':'.$key, $value, $tpl);
	    endforeach;
	    return $tpl;
	}

	 /*
	 * Send mail from data struct
	 */
	public function sendMail($datas){

	    $transport = Swift_MailTransport::newInstance();
	    //$transport = Swift_SendmailTransport::newInstance('/usr/sbin/sendmail -bs');
//	    $transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', '465', 'ssl')
//		  ->setUsername('xxx@gmail.com')
//		  ->setPassword('xxxyyyzzz');
	    $mailer = Swift_Mailer::newInstance($transport);
	    $message = Swift_Message::newInstance($datas['subject'])
	    ->setFrom(array($datas['from']['mail'] => $datas['from']['name']))
	    ->setTo($datas['to'])
	    ->setContentType("text/html")
	    ->setBody($datas['body'])
	    ;
	    if(!empty($datas['qrcode'])){
	        // Create the attachment
	        // * Note that you can technically leave the content-type parameter out
	        $attachment = Swift_Attachment::fromPath($datas['qrcode']['path'], $datas['qrcode']['datatype'])->setFilename($datas['qrcode']['name']);
	        // Attach it to the message
	        $message->attach($attachment);
	    }

	    // Send the message
	    return $mailer->send($message);
	}
        
        protected function uploadImage($image, $newImageName)
        {
            //if they DID upload a file...
            if($image['name'])
            {
                    //if no errors...
                    if(!$image['error'])
                    {                            
                            //move it to where we want it to be
                            if(move_uploaded_file($image['tmp_name'], $newImageName))
                            {
                                return true;
                            }                                                        
                    }                  
            }

            return false;
        }

	protected function jsonResponse($data, $statusCode, $key = '')
	{
		$response = $this->app->response();
		$response['Content-Type'] = 'application/json';
		$response['X-Powered-By'] = POWERED_BY;
		$response->status($statusCode);
		$data['accessKey'] = $key;

		if(!$this->isJson($data))
			$data = json_encode($data);

		$response->body($data);
	}

	protected function isJson($string) {
		if(is_string($string))
		{
			json_decode($string);
			return (json_last_error() == JSON_ERROR_NONE);
		}
		return false;
	}
}