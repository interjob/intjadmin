<!DOCTYPE html>
<html>
	<head>
		<title>:title</title>
		<meta name="description" content=":description"/>
		<meta name="keyword" content=":keywords" />
		<meta name="author" content="Interjob">                
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
                
                <link type="text/css" rel="stylesheet" href="/resources/css/reset.css"/>
                <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">    -->
                
                <link type="text/css" rel="stylesheet" href="/resources/css/responsive-core.css"/>
                <link type="text/css" rel="stylesheet" href="/resources/css/font-awesome.min.css"/>
                <link type="text/css" rel="stylesheet" href="/resources/css/style.css"/>
                <link rel="stylesheet" type="text/css" href="/resources/js/rs-plugin/css/settings.css" media="screen" />
                                
		<link rel="stylesheet" href="/resources/css/:style.css" type="text/css" media="screen" charset="utf-8">	
            
	
                <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
                <link rel="icon" href="/favicon.ico" type="image/x-icon">
	</head>
	<body>
