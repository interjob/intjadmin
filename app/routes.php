<?php

class Route{

  private function controllerExists($controllerName){
    if(file_exists(BASE_PATH."/controllers/$controllerName.php")){
      //if(class_exists($controllerName))
        return true;
    }
    return false;
  }

  protected function controller($shortName, $app, $db = null, $params = null)
  {
    list($shortClass, $shortMethod) = explode('/', $shortName, 2);

    $controller   = ucfirst($shortClass).'Controller';
    $fullName     = sprintf('%s::%s', $controller, $shortMethod);

    if( $this->controllerExists($controller) ){

      require BASE_PATH."/controllers/$controller.php";

        $arguments  = func_get_args();
        array_shift($arguments); //Remove the class and method name
        $reflect    = new ReflectionClass($controller);

//        if(!empty($params))
//        {
//           $arguments = array_merge($arguments, $params);
//        }

        if(func_num_args() > 0) {
          $instance = $reflect->newInstanceArgs($arguments);
        }
        else {
          $instance = $reflect->newInstance();
        }

        $bool = method_exists($instance, 'beforeAction');
        if(method_exists($instance, 'beforeAction'))
          call_user_func_array(array($instance, 'beforeAction'), array($shortMethod));

        $rt = array($instance, $shortMethod);
        return $rt;
    }
  }

  public function routeExecute($app, $slug, $params, $db = null){
    // $request = Slim::getInstance()->request();
    // $wine = json_decode($request->getBody());
    $getParameters = null;
    if(isset($_GET["data"]))
    {
      $getParameters = $_GET["data"];
      $getParameters = json_decode($getParameters);
      $getParameters = (array)$getParameters->params;
    }

    $slimRequest = $app->request()->getBody();
    $slimRequest = json_decode($slimRequest);

    if(isset($slimRequest->params))
      $slimRequest = (array)$slimRequest->params;

    if(is_array($slimRequest))
    {
      $params = array_merge($params, $slimRequest);
    }
    if(is_array($getParameters))
    {
      $params = array_merge($params, $getParameters);
    }

    call_user_func( $this->controller($slug, $app, $db, $params) );
    // try{
    //   call_user_func( $this->controller($slug, $app, $db, $params) );
    // } catch (Exception $e) {
    //   $app->redirect('/not-found');
    // }
  }
}

$Route = new Route();

$app->map('/hello', function() use($app, $db, $Route){
    $slimRequest = $app->request()->getBody();
    $slimRequest = json_decode($slimRequest);
    $slimRequest = (array)$slimRequest->params;
    echo "Hello ".$slimRequest['nome']." ".$slimRequest['cognome'];
})->via('IWANT');

//VIEWS
$app->get('/', function() use($app, $db, $Route){
    $Route->routeExecute($app, "Public/home", array(), $db);
});
$app->get('/not-found',function(){
  echo 'Not Found';
});
$app->get('/hashing',function(){
  echo password_hash("rasmuslerdorf", PASSWORD_DEFAULT)."\n";
  echo "<br/>".hash(CRYPT_METHOD, "Password01");
  echo "<br/>".crypt("Password01",PASSWORD_SALT);
});

/*
$app->get('/culotest/:get1/:get2',function($get1,$get2)use($app,$db,$Route){
  $Route->routeExecute($app, "culotest/scriviculo", array(0=>$get1,1=>$get2),$db);  
});
 */


//registration and login
$app->get('/registration', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Public/registration", array(), $db);
});
$app->get('/login', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Public/login", array(), $db);
});
$app->get('/lost-password', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Public/lostPassword", array(), $db);
});
$app->get('/find', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Public/find", array(), $db);
});
$app->get('/set-new-password/:accessKey', function($accessKey) use($app, $db, $Route){    
  $Route->routeExecute($app, "Reserved/setNewPasswordForm", array('accessKey' => $accessKey), $db);
});
$app->get('/editRegistrationData/:accessKey', function($accessKey) use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/editRegistrationData", array("accessKey"=>$accessKey), $db);
});

$app->post('/updateRegistrationData', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/updateRegistrationData", array(), $db);
});

$app->get('/confirmAccount/:encodedEmail', function($encodedEmail) use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/confirmAccount", array('encodedEmail' => $encodedEmail), $db);
});

//static pages
$app->get('/privacy', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/privacy", array(), $db);
});
$app->get('/sitemap', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/sitemap", array(), $db);
});
$app->get('/about_us', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/about_us", array(), $db);
});
$app->get('/services', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/services", array(), $db);
});
$app->get('/faq', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/faq", array(), $db);
});
$app->get('/terms_and_conditions', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/terms_and_conditions", array(), $db);
});
$app->get('/credits', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/credits", array(), $db);
});

$app->get('/personal', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/personal", array(), $db);
});
$app->get('/company', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/company", array(), $db);
});
$app->get('/reseller', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/reseller", array(), $db);
});


//dinamic pages

$app->get('/swap', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/swap", array(), $db);
});
$app->get('/work', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/work", array(), $db);
});
$app->get('/promo', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/promo", array(), $db);
});

// search page
$app->get('/search', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/search", array(), $db);
});
$app->post('/search', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/searchResult", array(), $db);
});




//company pages

$app->get('/accountDashboard/:accessKey', function($accessKey) use($app, $db, $Route){    
  $Route->routeExecute($app, "Reserved/accountDashboard", array("accessKey"=>$accessKey), $db);
});

$app->get('/addCompany/:accessKey', function($accessKey) use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/addCompany", array("accessKey"=>$accessKey), $db);
});


$app->post('/removeCompany', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/removeCompany", array(), $db);
});


$app->get('/editCompanyData/:company_slug/:accessKey', function($companySlug, $accessKey) use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/editCompanyData", array( "companySlug"=>$companySlug, "accessKey"=>$accessKey), $db);
});
$app->post('/updateCompanyData', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/updateCompanyData", array(), $db);
});

$app->get('/editCompany/:company_slug/:accessKey', function($companySlug, $accessKey) use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/editCompanyData", array("companySlug"=>$companySlug,"accessKey"=>$accessKey), $db);
});
$app->get('/editAddresses/:company_slug/:accessKey', function($companySlug, $accessKey) use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/editAddresses", array("companySlug"=>$companySlug,"accessKey"=>$accessKey), $db);
});

$app->post('/removeAdressCompany', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/removeAdressCompany", array(), $db);
});

$app->post('/addAdressCompany', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/addAdressCompany", array(), $db);
});

$app->get('/editPromos/:company_slug/:accessKey', function($companySlug, $accessKey) use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/editPromos", array("companySlug"=>$companySlug,"accessKey"=>$accessKey), $db);
});
$app->get('/editJobs/:company_slug/:accessKey', function($companySlug, $accessKey) use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/editJobs", array("companySlug"=>$companySlug,"accessKey"=>$accessKey), $db);
});
$app->get('/editSwaps/:company_slug/:accessKey', function($companySlug, $accessKey) use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/editSwaps", array("companySlug"=>$companySlug,"accessKey"=>$accessKey), $db);
});



/*
$app->get('/testCompany', function($accessKey) use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/testCompany", array("accessKey"=>$accessKey), $db);
});
*/


//images

$app->get('/cropPopOver', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Public/cropPopOver", array(), $db);
});

$app->post('/accountImgCrop', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/accountImgCrop", array(), $db);
});

$app->post('/companyHeaderImgCrop', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/companyHeaderImgCrop", array(), $db);
});

$app->post('/uploadImage/:accessKey', function($accessKey) use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/uploadImage", array("accessKey"=>$accessKey), $db);
});

//paypal pages
$app->get('/paypal', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Reserved/paypalForm", array(), $db);
});
$app->post('/paypal', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Reserved/paypal", array(), $db);
});
$app->get('/paypalCancel', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Reserved/paypalCancel", array(), $db);
});
$app->get('/paypalConfirm', function() use($app, $db, $Route){    
  $Route->routeExecute($app, "Reserved/paypalConfirm", array(), $db);
});

//API CALLS

$app->post('/login', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/login", array(), $db);
});

$app->post('/register', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Public/register", array(), $db);
});

$app->post('/newCompany', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/newCompany", array(), $db);
});

$app->post('/get-new-password', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Public/getNewPassword", array(), $db);
});

$app->post('/set-new-password', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/setNewPassword", array(), $db);
});

$app->post('/allCategory', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Public/categoryList", array(), $db);
});

$app->post('/authorizeMe', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/showCompanyPage", array(), $db);
});

$app->post('/logout', function() use($app, $db, $Route){
  $Route->routeExecute($app, "Reserved/logout", array(), $db);
});

if(DEBUG)
{
    $app->get('/resetDB', function()use($db){
        $db->reset();
        echo "DB Reset Done!";
    });
    
    $app->get('/regenerateAllCompanies', function()use($app, $db, $Route){
    $Route->routeExecute($app, "Reserved/regenerateAllCompanies", array(), $db);
        echo "company Reset Done!";
    });
}
