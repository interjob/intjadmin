<?php
//*class Auth
/**
	This is a wrapper class separate the auth module from core code and change auth method effortless
*/
class Auth extends baseModel
{
	//*AccessAuth object
	/**
		$accessAuth contain authetication object init with this wrapper class in construct method, this is mandatory data member
	*/
	protected $accessAuth;

	//* string
	/**
		$key is the passed key to authorize every action, can be substitude with the login fields, this data member could be usefull to multiple checks
	*/
	public $key = null;

        //* integer
	/**
		$userID is the primary key of authuser table
	*/
	public $userID = null;
        
	//* string
	/**
		$username is a part of login's data member mandatory to get accesskey
	*/
	public $username = null;

	//* string
	/**
		$password is a part of login's data member mandatory to get accesskey
	*/
	public $password = null;

	//*construct method
	/**
		Auth need all mandatory data for authentication class wrapped to initialize wrapped class instance in a data member
		\$db PDO Resource
		\$debug boolean
	*/
	function __construct($db, $debug)
	{
		$this->accessAuth = new T4\AccessAuth($db, $debug);
	}

	//* unique public method in the whole class to verify your authetication
	/**
		get accessCode it by passed parameters or use what you previously get to authorizer your action
		return true or false
	*/
	public function areYouAutheticated()
	{
		$data = null;
		if($this->getTheKey())
			$data = array('key' => $this->key);
		if($this->getTheLogin())
                {
                    $data = array('username' => $this->username, 'password' => $this->password);                    
                }
                                
		if(!is_null($data))
                {
                    $this->key = $this->accessAuth->autheticateMe($data);
                    $this->userID = $this->accessAuth->userID;
                }                
//		if(DEBUG)
//			$this->key = 'debugKey';

		return (!is_null($this->key)) ? true : false;
	}

	//*private method to get the key
	/**
		this method try to get the key from paramenter and check it from empty content and save it in $key data member
		returns true or false
	*/
	private function getTheKey()
	{
		if(is_null($this->key))
			return false;
		else
			return true;

	}
        
        //*public method to get the key
	/**
		this method reset the key from userID and return it
		returns new accessKey or false
	*/
	public function resetKey($userID)
	{
            return $this->accessAuth->resetKey($userID);
	}

	//*public method to set key data member
	/**
		this method set data member key with passed data
	*/
	public function setTheKey($key)
	{
		$this->key = $key;
	}

	//public method to get userID
	/**
		this method get from extended class the userID parameter and return it
	*/
	public function getUserID()
	{
		return $this->accessAuth->userID;
	}

	//*private method to check parameters
	/**
		this method check passed parameters
		return true or false
	*/

	private function getTheLogin()
	{
                if (!is_null($this->username) && !is_null($this->password))
                {
                    return true;
                }
		if(isset($_REQUEST['username']) && isset($_REQUEST['password']))
		{
			if(!empty($_REQUEST['username']) && !empty($_REQUEST['password']))
			{
				$this->username = $_REQUEST['username'];
				$this->password = $_REQUEST['password'];
				return true;
			}
		}

		$this->username = null;
		$this->password = null;
		return false;
	}
        
        //* public static method to insert authuser
	/**
		this method insert new authuser with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
					'email'                     =>  $data['email'],
                                        'pass'                      =>  $data['pass'],
                                        'authlvl'                   =>  1,
                                        'lastlogin'                 => '19900219 14:22:45',
					'createdat'                 => '19900219 14:22:45',
					'updatedat'                 => '19900219 14:22:45',
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function insertAuthUser($db, $params, $userID)
	{
		$rs = self::get($db, $params, 'authuser', $userID);
                if(isset($rs[0]))
			return ALREADY_IN;
		else
                {
                    $newUserID = parent::insert($db, $params, 'authuser', $userID);                    
                    return $newUserID;
                }			
	}

	//* public static method to update authuser
	/**
		this method update existing authuser with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
					'email'                     =>  $data['email'],
                                        'pass'                      =>  $data['pass'],
                                        'authlvl'                   =>  1,
                                        'lastlogin'                 => '19900219 14:22:45',
					'createdat'                 => '19900219 14:22:45',
					'updatedat'                 => '19900219 14:22:45',
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function updateAuthUser($db, $params, $conditions, $userID)
	{
		return parent::update($db, $params, 'authuser', $conditions, $userID);
	}

	//*public static method to delete authuser
	/**
		this method delete one entity row
		$db : PDO resource to connect and retrieve data
		$id : mandatory for find what to delete
		$userID : it's identifier of user that do this action
		return true or false if the operation do its job or not
	*/
	static public function deleteAuthUser($db, $params = array(), $userID)
	{
		return parent::delete($db, $params, 'authuser', $userID);
	}

	//*public static method to get authuser
	/**
		this method return entity list or if id parameter is not null get details of one authuser entity
		$db : PDO resource to connect and retrieve data
		$id : optional parameter, if it is set get single authuser entity
		$userID : it's identifier of user that do this action
		return list of authuser or single authuser
	*/
	static public function getAuthUser($db, $params = array(), $userID)
	{
		return parent::get($db, $params, 'authuser', $userID);
	}
        
        //* public static method to insert authtoken
	/**
		this method insert new authuser with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
					'authuserid'                =>  $userID,                                        
                                        'accessKey'                 => '235_asuiasa4dv',
					'createdat'                 => '19900219 14:22:45',
					'updatedat'                 => '19900219 14:22:45',
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/        
	static public function insertAuthToken($db, $userID)
	{
		$rs = self::get($db, array('authUserID' => $userID ), 'authtoken', $userID);
                if(isset($rs[0]))
			return ALREADY_IN;
		else
                {
                    $newKey = $userID.'_'.uniqid();
                    $authToken = parent::insert($db, array( 'authUserID' => $userID, 'accessKey' => $newKey ), 'authtoken', $userID);
                    return $newKey;
                }			
	}        
}