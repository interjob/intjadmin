<?php

class Log extends baseModel
{

	static public function logIt($db, $query, $authuserid)
	{
		//Set createdat parameter
		$params["loggedat"] = date("Y-m-d H:i:s");
		$params["action"] = $query;
		$params["authuserid"] = $authuserid;
		$setColumns = implode(", ", array_keys($params));
		$queryParams = $db->prepareParameters($params);
		$setValues = implode(", ", array_keys($queryParams));

		$query = "INSERT INTO log ( $setColumns ) VALUES ($setValues) RETURNING id";

		$return = MyPDO::myExec($db, $query, $queryParams, $authuserid, false);

		if(isset($return['id']) || isset($return[0]['id']))
			return true;
		else
			return false;
	}

}