var accessKey="";

    
Form.fillCategoryFiled = function(data,textStatus,jqXHR){
    //console.log(data);
    $.each(data.data, function (i, item) {
        $('#categories').append($('<option>', { 
            value: item.id,
            text : item.value 
        }));
    });
};

Form.loadScript = function(data,textStatus,jqXHR){
    console.log(data);
};

Form.listCategory = function(){
    var input = {};
    input.params={};
    input.params.accessKey =  accessKey;
    input=JSON.stringify(input);        

    var defaultData ={
            url 		: "/allCategory",
            methodForm 		: 'POST',
            beforeSendHandler 	: false,         
            errorHandler 	: 'Form.errorHandler',             
            successHandler 	: 'Form.fillCategoryFiled(data,textStatus,jqXHR)',  
            completeHandler	: false,
            formData		: input,
            dataType		: "json",
            cache		: false,
            contentType		: false,
            processData		: false,
            crossDomain		: false
    };        
    
    Form.ajaxCall(defaultData);
};
    
// search control
        //parte spinner
        //get ajax companies
        //popolare .htmlò()
        //div nascosto con badge da riempire nei duplicati
        
        
/*
$(".submitSearch").click(function(){
   var keyword = $("#keywords").val();    
   var location = $("#locations option:selected").val();
   var category = $("#locations option:selected").val();    
   
                   var input = {};
                input.params={};
                input.params.accessKey =  accessKey;
                input=JSON.stringify(input);        
        
		var defaultData ={
			url 			: "/searchResult",
			methodForm 		: 'POST',
			beforeSendHandler 	: false,         
			errorHandler 		: 'Form.errorHandler',             
			successHandler 		: 'Form.submitNoErrorsSearch(data,textStatus,jqXHR)',  
			completeHandler		: false,
			formData		: input,
			dataType		: "json",
			cache			: false,
			contentType		: false,
			processData		: false,
			crossDomain		: false
		};        
        Form.ajaxCall(defaultData);
            
});*/


Form.errorHandler = function (jqXHR, textStatus, errorThrown){
    
    console.log(jqXHR.status);
  
        switch (jqXHR.status){
                        
                            
            case 401: //force logout, reset cookie, redirect /
                        Form.logOut();
                        break;
            case 409: 
                        alert(jqXHR.responseJSON.message);
                        break;
            case 410:  alert(jqXHR.responseJSON.message);
                        break;
            case 500:
            case 501:
            case 502:
            case 503:
            case 504:
            case 505:
               case 509: 
                        alert(jqXHR.responseJSON.message);
                        Form.logOut();                        
                        break;


            default: 
               // Form.redirect("/");
            break;            
        }

    
    //TO DO do log via ajax
    //console.log(jqXHR);
}

Form.submitNoErrorsSearch = function(data, textStatus, jqXHR){
    //console.log(textStatus);   
    //alert(textStatus);
    if(!data.success){
        alert("xx");
    }
    else
    {
        $(".hideSpinner").hide();
    }
};


    
    
//link reserved page

    
//revolution slider

revSlider("#revo-slider");

//autocomplete chrome force off
/*
    if (navigator.userAgent.toLowerCase().indexOf('chrome') >= 0) {
        setTimeout(function () {
            document.getElementById('autocomplete').autocomplete = 'off';
        }, 1);
    }
*/


// google maps geolocalization

// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',  
  administrative_area_level_2: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
  // Create the autocomplete object, restricting the search to geographical
  // location types.
  autocomplete = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
      {types: ['geocode']});

  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

// [START region_fillform]
function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    //document.getElementById(component).disabled = false;
  }

  // Get each component of the address from the place details
  // and fill the corresponding field on the form.
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }    
  }
  
  console.log(place.geometry.location);
  
  document.getElementById("latgeo").value = place.geometry.location.J;
  document.getElementById("longgeo").value = place.geometry.location.M;
  
}
// [END region_fillform]

// [START region_geolocation]
// Bias the autocomplete object to the user's geographical location,
// as supplied by the browser's 'navigator.geolocation' object.
function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}



$(window).load(function() {
       
    $(".accessVal").each(function(){
        $(this).attr("href",$(this).attr("href")+"/"+accessKey);
    });
      
});

function revSlider(target){
            $(target).revolution(
				{
					delay:1500,
					startwidth:1170,
					startheight:450,

					onHoverStop:"off",						// Stop Banner Timet at Hover on Slide on/off

					thumbWidth:100,							// Thumb With and Height and Amount (only if navigation Tyope set to thumb !)
					thumbHeight:50,
					thumbAmount:3,

					hideThumbs:0,
					navigationType:"none",				// bullet, thumb, none
					navigationArrows:"solo",				// nexttobullets, solo (old name verticalcentered), none

					navigationStyle:"round",				// round,square,navbar,round-old,square-old,navbar-old, or any from the list in the docu (choose between 50+ different item), custom


					navigationHAlign:"center",				// Vertical Align top,center,bottom
					navigationVAlign:"bottom",					// Horizontal Align left,center,right
					navigationHOffset:30,
					navigationVOffset: 40,

					soloArrowLeftHalign:"left",
					soloArrowLeftValign:"center",
					soloArrowLeftHOffset:0,
					soloArrowLeftVOffset:0,

					soloArrowRightHalign:"right",
					soloArrowRightValign:"center",
					soloArrowRightHOffset:0,
					soloArrowRightVOffset:0,

					touchenabled:"on",						// Enable Swipe Function : on/off


					stopAtSlide:0,							// Stop Timer if Slide "x" has been Reached. If stopAfterLoops set to 0, then it stops already in the first Loop at slide X which defined. -1 means do not stop at any slide. stopAfterLoops has no sinn in this case.
					stopAfterLoops:0,						// Stop Timer if All slides has been played "x" times. IT will stop at THe slide which is defined via stopAtSlide:x, if set to -1 slide never stop automatic

					hideCaptionAtLimit:0,					// It Defines if a caption should be shown under a Screen Resolution ( Basod on The Width of Browser)
					hideAllCaptionAtLilmit:0,				// Hide all The Captions if Width of Browser is less then this value
					hideSliderAtLimit:0,					// Hide the whole slider, and stop also functions if Width of Browser is less than this value


					fullWidth:"on",

					shadow:0								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows -  (No Shadow in Fullwidth Version !)

				});
}
    


$( document ).ready(function() {
      
      /* precompila i campi */
      /*
    $(".errorLabel").prev().val(
            $(".errorLabel").html()
        );*/
    $(".errorLabel").text();      
      
      
    if ($.cookie("accessKey")&&($.cookie("accessKey")!=null)){
        accessKey = $.cookie("accessKey");
    }
    
   // Form.listCategory();

    //login / account dashboard
    viewController.headerLogged();

    //logout
    
    $("#logoutClick").click(function(){
        
        var input = {};
        input.params={};
        input.params.accessKey =  accessKey;
        input=JSON.stringify(input);        

        var defaultData ={
                url 				: "/logout",
                methodForm 			: 'POST',
                beforeSendHandler 	: false,
        /*	errorHandler 		: 'Form.logOut()',           */
                errorHandler 		: 'Form.errorHandler',  
                successHandler 		: "Form.logOut()",
                completeHandler		: false,     
                formData			: input,
                dataType			: "json",
                cache				: false,
                contentType			: false,
                processData			: false,
                crossDomain			: false
        };        
        Form.ajaxCall(defaultData);
        
    });
});