<!DOCTYPE html>

<html>
	<head>
		<meta charset="utf-8">
	</head>
	<body style="background:#f7f7f7; margin:0; padding:0;">
		<center>
			<table cellpadding="0" cellspacing="0" width="640px" border="0" style="background:#fff; border-left: solid 1px #33F9B6; border-right: solid 1px #33F9B6;">
				<tr height="25px"></tr>
				<tr>
					<td>
						<img width="500px" style="display:block; margin:0 auto;" src=":base_url/img/logo.jpg">
					</td>
				</tr>
				<tr height="50px"></tr>
				<tr>
					<td>
                                            <h2 style="margin:0; padding:0 20px; font-family: Arial, Halvetica, sans-serif; color:#4d4d4d; font-weight:200; font-size:30px; text-align:left;"><a href=":base_url/confirmAccount/:encodedemail">Clicca qui</a> per confermare la tua email.</h2>
					</td>
				</tr>
				<tr>
					<td>					
						<p style="margin:0; padding:20px; font-family: Arial, Halvetica, sans-serif; color:#4d4d4d; font-weight:200; font-size:16px; text-align:left; line-height:25px;">Staff LinkForAll</p>
					</td>
				</tr>
			</table>
		</center>
	</body>
</html>