<?php

class Language extends baseModel{

	public $language;

	function __construct($lang){
		$this->language = $lang;
	}

	public function getTranslation(){
		require_once "../src/i18n/{$this->language}.php";
		return $translation;
	}
        
        //LANGUAGE
        
        //* public static method to insert languages
	/**
		this method insert new languages with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
                                        'language_code'         => 'IT',
                                        'flag'                  => 'path/to/image',
                                        'language'              => 'languages_name_it',                                        
					'createdat'             => '19900219 14:22:45',
					'updatedat' 		=> '19900219 14:22:45',
                                        'active'                => 1,
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function insertLanguage($db, $params, $userID)
	{
		$rs = self::get($db, $params, 'languages_i18n', $userID);

		if(isset($rs[0]))
			return ALREADY_IN;
		else
			return parent::insert($db, $params, 'languages', 'id', $userID);
	}

	//* public static method to update languages
	/**
		this method update existing languages with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
                                        'language_code'         => 'IT',
                                        'flag'                  => 'path/to/image',
                                        'language'              => 'languages_name_it',                                        
					'createdat'             => '19900219 14:22:45',
					'updatedat' 		=> '19900219 14:22:45',
                                        'active'                => 1,
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function updateLanguage($db, $params, $conditions, $userID)
	{
		return parent::update($db, $params, 'languages', 'id', $conditions, $userID);
	}

	//*public static method to delete languages
	/**
		this method delete one entity row
		$db : PDO resource to connect and retrieve data
		$id : mandatory for find what to delete
		$userID : it's identifier of user that do this action
		return true or false if the operation do its job or not
	*/
	static public function deleteLanguage($db, $params = array(), $userID)
	{
		return parent::delete($db, $params, 'languages', $userID);
	}

	//*public static method to get languages
	/**
		this method return entity list or if id parameter is not null get details of one language entity
		$db : PDO resource to connect and retrieve data
		$id : optional parameter, if it is set get single languages_i18n entity
		$userID : it's identifier of user that do this action
		return list of language or single language
	*/
	static public function getLanguage($db, $params = array(), $userID)
	{
		return parent::get($db, $params, 'languages', $userID);
	}
        
        
        //LANGUAGE I18N
        
        //* public static method to insert languages_i18n
	/**
		this method insert new languages_i18n with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
                                        'code_key'              => 'profiles_types_title_free',
                                        'language_code'         => 'IT',
                                        'value'                 => 'Free',                                        
					'createdat'             => '19900219 14:22:45',
					'updatedat' 		=> '19900219 14:22:45',
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function insertLanguagei18n($db, $params, $userID)
	{
		$rs = self::get($db, $params, 'languages_i18n', $userID);

		if(isset($rs[0]))
			return false;
		else
			return parent::insert($db, $params, 'languages_i18n', 'id', $userID);
	}

	//* public static method to update languages_i18n
	/**
		this method update existing languages_i18n with passed parameters
		<pre>
		$db : PDO resource to connect and modify data
		$params : parameters to update
				Ex.
				array(
                                        'code_key'              => 'profiles_types_title_free',
                                        'language_code'         => 'IT',
                                        'value'                 => 'Free',                                        
					'createdat'             => '19900219 14:22:45',
					'updatedat' 		=> '19900219 14:22:45',
				)
		$userID : it's identifier of user that do this action
		</pre>
		return number of affected rows
	*/
	static public function updateLanguagei18n($db, $params, $conditions, $userID)
	{
		return parent::update($db, $params, 'languages_i18n', 'id', $conditions, $userID);
	}

	//*public static method to delete languages_i18n
	/**
		this method delete one entity row
		$db : PDO resource to connect and retrieve data
		$id : mandatory for find what to delete
		$userID : it's identifier of user that do this action
		return true or false if the operation do its job or not
	*/
	static public function deleteLanguagei18n($db, $params = array(), $userID)
	{
		return parent::delete($db, $params, 'languages_i18n', $userID);
	}

	//*public static method to get languages_i18n
	/**
		this method return entity list or if id parameter is not null get details of one language entity
		$db : PDO resource to connect and retrieve data
		$id : optional parameter, if it is set get single languages_i18n entity
		$userID : it's identifier of user that do this action
		return list of language or single language
	*/
	static public function getLanguagei18n($db, $params = array(), $userID)
	{
		return parent::get($db, $params, 'languages_i18n', $userID);
	}
}
