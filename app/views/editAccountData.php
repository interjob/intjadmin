<div id="iscriversi" class="content">
   
    <h2><b></b>Modifica i tuoi dati tramite il <b>form qui sotto.</b></h2><br /><br />

    <div class="registrazione">
        <form class="registration" name="register_form" id="register_form" action="/updateRegistrationData" method="post" enctype="multipart/form-data">
                <fieldset class="clearfix">
                    
                    <!--
                    <div class="field left clearfix :error_email">
                        <label for="email">E-mail*</label>
                        <input id="email" name="email" tabindex="1" maxlength="64" class="required" value=":email_value" type="email" />
                        <span class="errorLabel">:email</span>
                    </div>
                    -->
                    
                    <div class="field left clearfix :error_password">
                        <label for="password">Password*</label>
                        <input id="password" name="password" tabindex="1" maxlength="64" class="required" value=":password_value" type="password" />                        
                        <label for="password-recheck">Riscrivi la password*</label>
                        <input id="password-recheck" name="password-recheck" tabindex="1" maxlength="64" class="required" value=":password_value" type="password" />
                        <span class="errorLabel">:password</span>
                    </div>
                    
                    
                    <div class="field left clearfix :error_name">
                        <label for="name">Nome*</label>
                        <input id="name" name="name" tabindex="1" maxlength="64" class="required" value=":name_value" type="text" />
                        <span class="errorLabel">:name</span>
                    </div>
                    <div class="field :error_surname">
                        <label for="surname">Cognome*</label>
                        <input id="surname" name="surname" tabindex="2" maxlength="64" class="required" type="text" value=":surname_value" />
                        <span class="errorLabel">:surname</span>
                    </div>
                    
                      <div class="field :error_birthplace">
                        <label for="birthplace">birthplace*</label>
                        <input id="birthplace" name="surname" tabindex="2" maxlength="64" class="required" type="text" value=":birthplace_value" />
                        <span class="errorLabel">:birthplace</span>
                    </div>
                    
                      <div class="field :error_birthdate">
                        <label for="birthdate">birthdate*</label>
                            <input
                        type="date"
                        id="birthdate"
                        name="birthdate"
                        popup="dd MMM yyyy"
                        options="dateOptions"
                        opened="opened"
                        custom-datepicker
                        value=":birthdate_value"
                        />
                        <span class="errorLabel">:birthdate</span>
                    </div>
                    
                    
                      <div class="field :error_sex">
                        <label for="sex">Sesso*</label>
                                <label class="radio-inline">
                                  <input type="radio" class="changeSex" :sexMale name="changeSex" id="inlineMale" value="M"> M
                                </label>
                                <label class="radio-inline">
                                  <input type="radio" class="changeSex" :sexFemale name="changeSex" id="inlineFemale" value="F"> F
                                </label>
                        <input id="sex" type="hidden" name="sex" value=":sex_value" />
                        <span class="errorLabel">:sex</span>
                    </div>                    
                    
                    
                      <div class="field :error_phone">
                        <label for="phone">telefono*</label>
                        <input id="phone" name="phone" tabindex="2" maxlength="64" class="required" type="text" value=":phone_value" />
                        <span class="errorLabel">:phone</span>
                    </div>                    
                    
                      <div class="field :error_address">
                        <label for="address">Indirizzo*</label>
                        <input id="address" name="address" tabindex="2" maxlength="64" class="required" type="text" value=":address_value" />
                        <span class="errorLabel">:address</span>
                    </div>                    
                    
                      <div class="field :error_address">
                        <label for="address_civic">Numero civico*</label>
                        <input id="address_civic" name="address_civic" tabindex="2" maxlength="64" class="required" type="text" value=":address_civic_value" />
                        <span class="errorLabel">:address_civic</span>
                    </div>         
                    
                    <div class="field :error_zipcode">
                        <label for="zipcode">Cap*</label>
                        <input id="zipcode" name="address" tabindex="2" maxlength="64" class="required" type="text" value=":zipcode_value" />
                        <span class="errorLabel">:zipcode</span>
                    </div>                    
                    
                    <div class="field :error_city">
                        <label for="city">Citta*</label>
                        <input id="city" name="city" tabindex="2" maxlength="64" class="required" type="text" value=":city_value" />
                        <span class="errorLabel">:city</span>
                    </div>                    
                    
                    <div class="field :error_states">
                        <label for="states">Provincia*</label>
                        <input id="states" name="states" tabindex="2" maxlength="64" class="required" type="text" value=":states_value" />
                        <span class="errorLabel">:states</span>
                    </div>                    
                    
                    <div class="field :error_registration_country">
                        <label for="registration_country">Paese*</label>
                        <input id="registration_country" name="registration_country" tabindex="2" maxlength="64" class="required" type="text" value=":registration_country_value" />
                        <span class="errorLabel">:registration_country</span>
                    </div>                    
                    
                    <div class="field :error_vatcode">
                        <label for="vatcode">Codice fiscale*</label>
                        <input id="vatcode" name="vatcode" tabindex="2" maxlength="64" class="required" type="text" value=":vatcode_value" />
                        <span class="errorLabel">:vatcode</span>
                    </div>                    

                </fieldset>

                <div class="field privacy :error_newsletter">
                    <label class="newsletter_label">Iscriviti alla newsletter</label>
                    <input data-rules="newsletter" id="newsletter"  name="newsletter" class="" tabindex="11" type="checkbox" :newsletterActive  />
                    <span class="errorLabel">:newsletter</span>
                </div>
            
                <div class="field service"></div>
                <p style="font-size:13px;">Tutti i campi con * sono obbligatori.</p>
                <a href="#" class="button registrar">Modifica</a>
                <div class="loader"></div>
        </form>
        <input id="accountSlug" value=":surname_value" type="hidden">

        <!-- CONFERMA AVVENUTA ISCRIZIONE -->
        <h3 class="successful">:successMessage</h3>
        <!-- La tua registrazione è andata a buon fine. Tra poco riceverai una mail di conferma contenente le istruzioni. -->

        <!-- ERRORE REGISTRAZINOE -->
        <h3 class="mistake">:failureMessage</h3>
    </div>
    <div class="clear"></div>
</div>
